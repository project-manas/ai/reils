import numpy as np
import tensorflow as tf

from ..env.base import Spaces
from ..utils import tf_util as U


class Pd(object):
	"""
	A particular probability distribution
	"""

	def flatparam(self):
		raise NotImplementedError

	def mode(self):
		raise NotImplementedError

	def neglogp(self, x):
		# Usually it's easier to define the negative logprob
		raise NotImplementedError

	def kl(self, other):
		raise NotImplementedError

	def entropy(self):
		raise NotImplementedError

	def sample(self):
		raise NotImplementedError

	def logp(self, x):
		return - self.neglogp(x)


class PdType(object):
	"""
	Parametrized family of probability distributions
	"""

	def pdclass(self):
		raise NotImplementedError

	def pdfromflat(self, flat):
		return self.pdclass()(flat)

	def param_shape(self):
		raise NotImplementedError

	def sample_shape(self):
		raise NotImplementedError

	def sample_dtype(self):
		raise NotImplementedError

	def param_placeholder(self, prepend_shape, name=None):
		return tf.placeholder(dtype=tf.float32, shape=prepend_shape + self.param_shape(), name=name)

	def sample_placeholder(self, prepend_shape, name=None):
		return tf.placeholder(dtype=self.sample_dtype(), shape=prepend_shape + self.sample_shape(), name=name)


class RandomPd(Pd):
	"""
	A particular probability distribution
	"""

	def flatparam(self):
		pass

	def mode(self):
		return tf.constant([0], dtype=tf.float32)

	def neglogp(self, x):
		# Usually it's easier to define the negative logprob
		return tf.constant([0], dtype=tf.float32)

	def kl(self, other):
		return tf.constant([0], dtype=tf.float32)

	def entropy(self):
		return tf.constant([0], dtype=tf.float32)

	def sample(self):
		return tf.constant([0], dtype=tf.float32)

	def logp(self, x):
		return - self.neglogp(x)


class RandomPdType(PdType):
	"""
	Parametrized family of probability distributions
	"""

	def pdclass(self):
		raise NotImplementedError

	def pdfromflat(self, flat):
		return self.pdclass()(flat)

	def param_shape(self):
		raise NotImplementedError

	def sample_shape(self):
		raise NotImplementedError

	def sample_dtype(self):
		raise NotImplementedError

	def param_placeholder(self, prepend_shape, name=None):
		return tf.placeholder(dtype=tf.float32, shape=prepend_shape + self.param_shape(), name=name)

	def sample_placeholder(self, prepend_shape, name=None):
		return tf.placeholder(dtype=self.sample_dtype(), shape=prepend_shape + self.sample_shape(), name=name)


class CategoricalPdType(PdType):
	def __init__(self, ncat):
		self.ncat = ncat

	def pdclass(self):
		return CategoricalPd

	def param_shape(self):
		return [self.ncat]

	def sample_shape(self):
		return []

	def sample_dtype(self):
		return tf.int32


class DiagGaussianPdType(PdType):
	def __init__(self, size):
		self.size = size

	def pdclass(self):
		return DiagGaussianPd

	def param_shape(self):
		return [2 * self.size]

	def sample_shape(self):
		return [self.size]

	def sample_dtype(self):
		return tf.float32


class CategoricalPd(Pd):
	def __init__(self, logits):
		self.logits = logits

	def flatparam(self):
		return self.logits

	def mode(self):
		return U.argmax(self.logits, axis=-1)

	def neglogp(self, x):
		x = tf.cast(x, tf.int32)
		one_hot = tf.one_hot(x, self.logits.get_shape().as_list()[-1])
		return tf.nn.softmax_cross_entropy_with_logits_v2(
			logits=self.logits,
			labels=one_hot
		)

	def kl(self, other):
		a0 = self.logits - U.max(self.logits, axis=-1, keepdims=True)
		a1 = other.logits - U.max(other.logits, axis=-1, keepdims=True)
		ea0 = tf.exp(a0)
		ea1 = tf.exp(a1)
		z0 = U.sum(ea0, axis=-1, keepdims=True)
		z1 = U.sum(ea1, axis=-1, keepdims=True)
		p0 = ea0 / z0
		return U.sum(p0 * (a0 - tf.log(z0) - a1 + tf.log(z1)), axis=-1)

	def self_kl(self):
		other = CategoricalPd(tf.stop_gradient(tf.identity(self.logits)))
		return other.kl(self)

	def entropy(self):
		a0 = self.logits - U.max(self.logits, axis=-1, keepdims=True)
		ea0 = tf.exp(a0)
		z0 = U.sum(ea0, axis=-1, keepdims=True)
		p0 = ea0 / z0
		return U.sum(p0 * (tf.log(z0) - a0), axis=-1)

	def sample(self):
		u = tf.random_uniform(tf.shape(self.logits))
		diff = self.logits - tf.log(-tf.log(u))
		return tf.argmax(diff, axis=-1)

	@classmethod
	def fromflat(cls, flat):
		return cls(flat)


class DiagGaussianPd(Pd):
	def __init__(self, flat=None):
		if flat is not None:
			self.flat = flat
			mean, logstd = tf.split(axis=-1, num_or_size_splits=2, value=flat)
			self.mean = mean
			self.logstd = logstd
			self.std = tf.exp(logstd)
		else:
			pass

	def flatparam(self):
		return self.flat

	def mode(self):
		return self.mean

	def neglogp(self, x):
		return 0.5 * U.sum(tf.square((x - self.mean) / self.std), axis=-1) \
			   + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(x)[-1]) \
			   + U.sum(self.logstd, axis=-1)

	def kl(self, other):
		assert isinstance(other, DiagGaussianPd)
		return U.sum(other.logstd - self.logstd + (tf.square(self.std) + tf.square(self.mean - other.mean)) / (
				2.0 * tf.square(other.std)) - 0.5, axis=-1)

	def self_kl(self):
		other = DiagGaussianPd()
		other.mean = tf.stop_gradient(tf.identity(self.mean))
		other.std = tf.stop_gradient(tf.identity(self.std))
		return other.kl(self)

	def entropy(self):
		return U.sum(self.logstd + .5 * np.log(2.0 * np.pi * np.e), -1)

	def sample(self):
		return self.mean + self.std * tf.random_normal(tf.shape(self.mean))

	@classmethod
	def fromflat(cls, flat):
		return cls(flat)


def make_pdtype(ac_space):
	if isinstance(ac_space, Spaces.Continuous):
		assert len(ac_space.shape) == 1
		return DiagGaussianPdType(ac_space.shape[0])
	elif isinstance(ac_space, Spaces.Discrete):
		assert len(ac_space.shape) == 0
		return CategoricalPdType(ac_space.high)
	else:
		raise NotImplementedError


def shape_el(v, i):
	maybe = v.get_shape()[i]
	if maybe is not None:
		return maybe
	else:
		return tf.shape(v)[i]
