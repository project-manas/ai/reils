import time


class Analytics(object):
	def __init__(self, name: str = None):
		self.name = name
		self.i = 0
		self.start = time.time()

	def tick(self, tick_count: int = 1):
		self.i += tick_count
		diff = time.time() - self.start
		if diff >= 5:
			print('%s: %d fps' % (self.name, self.i / diff))
			self.start = time.time()
			self.i = 0
