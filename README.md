Reinforcement Learning Sandbox
===

# What is the domain/problem statement? 
Training of reinforcement learning algorithms can be thought of as a data-producer-consumer problem.
## Data producers
A set of agents that interact with the environment under a given policy to produce frames of experience.
## Data consumers
Produced frames are consumed by the training algorithm to optimize the policy. In case of on-policy optimization, this optimized policy is propagated back to the agents, whereas in the ideal case of off-policy, the production of data is independent of the consumption of data.
Current problems with the training of reinforcement learning agents
The task of reinforcement learning has certain complexities over supervised learning.

First, reinforcement learning algorithms are prone to take more time than supervised learning, as the algorithms need to optimize using a sparse scalar signal as opposed to much denser supervisor signal, this makes hyperparameter tuning far from trivial.
Second, the increased implementation complexity of systems that train reinforcement learning agents as opposed to supervised systems combined with the deterministic nature of deciding the exact technique that would best solve a given reinforcement learning problem.

In response to the above two problems, we propose a framework for rapid prototyping and testing of a wide variety of RL training techniques through seamless and rapid swapping of modular components, or their convenient development in case of new components, of the training system, to accelerate the research and development in a constantly evolving field, and also for the rapid deployment to production environments.

## Lack of reproducibility
We propose a standard template for concrete comparisons and reproducibility of reinforcement learning topologies.

# Existing approaches
The currently available open source RL frameworks are either focused of modularity allowing for quick prototyping (TensorForce), or they are optimized for speed, while obfuscating the code base(GA3C), not allowing further usage in different cases. 
Baselines by OpenAI are a set of RL algorithm implementation. They are the de facto for benchmark statistics for various algorithms. However, Baselines is not geared to be an extendable framework, but rather a rigid “baseline”.
BatchPPO is a recent implementation of the PPO algorithm with all the parallel agents synced and in parallel. 
GA3C is a hybrid CPU/GPU implementation of A3C. It uses a series of queues to and from the GPU for balancing training and collecting frames. It doesn’t parallelize the data producer and consumer, instead they are queued up to the GPU. It also isn’t extendable to other algorithms, it is built only for A3C.

# Our proposition
We propose a framework which can define any RL training system through the interaction of a set of few fundamental modules

## Brief Overview of Fundamental Modules
| Module      | Description                                                                                                                                                                                                                                                                                            |
|-------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Hypothesis  | A function approximator used for estimations. In the context of reinforcement learning, this can either be a policy estimator or a value estimator or both. Deep RL typically uses deep neural networks as the hypothesis.                                                                             |
| Environment | The problem environment in which the RL Agent trains.                                                                                                                                                                                                                                                  |
| Agent       | Uses observations from the environment, and utilizes it’s policy to determine the action to be taken in the Environment. In case of state-full agents, they also maintain state information.                                                                                                           |
| Explorer    | Coordinates agents to interact with environments so as to produces frames. A frame is a loosely defined collection of the observation, action, reward, etc associated with a particular step in the environment.                                                                                       |
| Experience  | Structured collections of frames. Represent the experience obtained by exploration of agents in environments.                                                                                                                                                                                          |
| Sampler     | Samples frames out of the Experience to be used by the Feeder. Samplers vary based on the policy they use to decide how to sampler.                                                                                                                                                                    |
| Feeder      | Preprocess data samples from experience so as to be compliant with the requirements of trainer. This approach of using a Feeder ensures no bottlenecks for the trainer, which is the most computationally expensive module.                                                                            |
| Trainer     | Has access to its own copy of the hypothesis which it iteratively updates based on the processed experience samples it is fed by the Feeder.                                                                                                                                                           |
| Propagator  | Especially useful in the case of on-policy algorithms, where we might have to update the agents’ hypothesis to be the same as the that of the Trainer.                                                                                                                                                 |
| Topology    | Very high level description of what type of a fundamental module to use as well as how they must interact with each other. With significant contributions to the development of a variety of instances of fundamental modules, a ReiLS architect may never need to leave this high level descriptions. |

## Technical Details of Fundamental Modules
| Module      | Implementation Details                                                                                                                                                                                                                                                                                  |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Hypothesis  | By overriding the behave function, one can take as parameter an observation and an optional state, and return logits, value prediction and the next state.                                                                                                                                              |
| Environment | Defines a standard interface to observe and perform actions in the environment, along with rendering among other things.                                                                                                                                                                                |
| Agent       | Each agent uses its assigned hypothesis, and feeds it an observation and samples from the result, using one among many action selectors. This sampling may be stochastic, greedy, or so on.                                                                                                             |
| Explorer    | Typically and especially in single agent environments, each agents and environments has a one-one mapping and runs on it’s own thread. In the current implementation, each thread has its own small FIFO queue into which frames are enqueued.                                                          |
| Experience  | ReiLS currently supports a variety of possible experiences which vary on the underlying data structure, on which experiences to discard when full, etc. The Topology launches a thread to dequeue frames from each of the explorer’s FIFO queues to fill the experience. Experience is buffered in RAM. |
| Sampler     | The Sampler supports multiple paradigms such as ordered, random, and currently also has support for sampling batches of frames.                                                                                                                                                                         |
| Feeder      | When used in conjunction with GPUs, the feeder can also perform the loading of the data samples to VRAM, so that the trainer does not have to block on this operation on the train thread.                                                                                                              |
| Trainer     | The Trainer runs on its own dedicated main thread for training and typically utilizes GPUs for maximum performance.                                                                                                                                                                                     |
| Propagator  | The Propagator is especially useful in the case of on-policy algorithms, where we might have to update the weights of the agents’ hypothesis to be the same as the weights of the Trainer’s hypothesis. Propagations happen on a separate thread preventing disruptions to Trainer and Agent.           |
| Topology    | In addition to defining the instances of fundamental modules, it also defines how the main training loop is performed.                                                                                                                                                                                  |

# Benefits of our approach
## Modularity
In addition to the obvious advantages of a modular design, the modular architecture allows the framework to be used a diverse variety of scenarios, as each key component has minimal cohesion with other modules allowing any given component to remain compatible with almost any other modules. For example, we can easily swap out QLearner with PolicyGradientsLearner.
Additionally, users can easily build their own module and plug it into the system as per their requirements.
With proper open-source collaboration, this can result in a rich collection of modules for a wide variety of situations and requirements similar to caffe’s zoo, except that the zoo would be a zoo of modules rather than models, leading to an exploding permutation of possible module combinations.

## Implementation
Concise and Implicit: Complex topologies such as prioritized experience replay with PPO can be described in under 100 lines of code!
Portable and reproducible, something that is very important in the research domain, can easily be achieved since it may be as trivial as sharing a single topology file and any custom modules.
Boilerplate code would be minimized.
Amount of time taken to implement is reduced drastically since most modules would already exist and since the low cohesion would imply convenient swapping of modules.
A ReiLS architect would not necessarily require an understanding of the underlying implementations of the modules he uses.

## Performance
Parallelism is handled implicitly. Due to the extensive queuing performance variations of any one module does not adversely affect the performance of the entire framework as a whole, which is not the case for the current frameworks.
Additionally, due to the increased use of standard implementations, the algorithms become increasingly efficient.
Since ReiLS leverages deep learning architectures such as Tensorflow, GPU acceleration is obtained out of the box.
Encourages research and development
The modularity, conciseness, and implicitness would be ideal for RL researchers since a wide variety of systems can quickly be experimented. Moreover, hyperparameter tuning and architectural changes would be a lot more rapid and clean.

# What are the existing loopholes with our solution?
Currently, the framework is built using Tensorflow, and so it has a build and run architecture, modifying certain parts of the framework during runtime is non-trivial.
No support for other frameworks like PyTorch, Caffe, Chainer, etc.
We currently use Python threads for CPU multitasking, this is not optimal in distributed scenarios with clusters where MPI would be more apt, especially with a large number of CPUs.
If the number of threads is way more than the number of cores of the PC, we encounter the law of diminishing returns, where the cost of context switching outweighs the speed gains due to parallelism. 
If using ReiLS to build an on-policy architecture, a possible scenario that may occur is when the size of the Experience is large, this introduces a policy lag with the current optimized policy, and the policy under which the frames were collected. The larger the experience, the larger the policy gap. A policy gap introduces noise in the gradients, slowing down convergence.

The experimental setup, i.e., benchmarking (Baselines, BatchPPO, GA3C)
Following is a series of metrics with which me compare our framework to existing frameworks.

## Wall time for popular algorithms
Training the same algorithm, with the same network and environment till they all reach optimal performance (max cumulative reward) measured against the wall time. This is a better measure as opposed to number of training steps.


## Time for reimplementation with and without ReiLS
A more qualitative analysis would be measuring the average time taken by a practitioner of RL to implement a series of RL algorithms with a multitude of varying factors with and without the use of ReiLS.


# Possible future improvements
Facilitating other deep learning frameworks (Caffe2, PyTorch, Chainer) and other environments (Gazebo, Starcraft 2). The framework was built for easy extensibility so incorporating these features should not be too difficult. 
Currently built for use on a single PC with multiple GPUs. Can’t handle distributed clusters. We will eventually build ReiLS to support this by using MPI for CPU parallelization, and Tensorflow Distributed for GPU parallelization.
	Building an RL zoo allowing for plug and play RL research with a multitude of modules.
An expansive saving framework allowing for the saving of weight, topology as well as experience, thereby allowing practitioners to share highly reproducible models.
