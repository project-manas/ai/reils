import threading

import tensorflow as tf

from sandblox import TFFunction, TFObject


class HypothesisPropagator(TFObject):
	def __init__(self, use_background_thread = False, scope_name: str = None):
		self.use_backgound_thread = use_background_thread
		self._should_stop = False
		if use_background_thread:
			self.thread = threading.Thread(target = self.on_start_propagation)
		super(HypothesisPropagator, self).__init__(scope_name)

	def start_propagation(self):
		if self.use_backgound_thread:
			self.thread.start()
		else:
			self.on_propagate()

	def on_start_propagation(self):
		while not self.should_stop():
			self.on_propagate()

	def on_propagate(self):
		print("Propagating")

	def should_stop(self):
		return self._should_stop

	def request_stop(self):
		self._should_stop = True

	def clear_stop(self):
		self._should_stop = False


class SinglePropagator(HypothesisPropagator):
	def __init__(self, destination_graph: TFFunction, source_graph: TFFunction):
		self.weight_update = destination_graph.assign_trainable_vars(source_graph)
		super(SinglePropagator, self).__init__()

	def on_propagate(self):
		tf.get_default_session().run(self.weight_update)


class MultiPropagator(HypothesisPropagator):
	def __init__(self, destination_graphs: [TFFunction], source_graph: TFFunction, scope_name: str = None):
		super(MultiPropagator, self).__init__(scope_name = scope_name)
		with tf.variable_scope(self.scope.rel):
			self.weight_updates = [hypothesis.assign_trainable_vars(source_graph) for hypothesis in
				destination_graphs]

	def on_propagate(self):
		super(MultiPropagator, self).on_propagate()
		for weight_update in self.weight_updates:
			tf.get_default_session().run(weight_update)


class CoordinatedPropagator(HypothesisPropagator):
	def __init__(self, msg_coordinator, sync_timeout = 2, use_background_thread = False):
		super(CoordinatedPropagator, self).__init__(use_background_thread)
		self.msg_coordinator = msg_coordinator
		self.sync_timeout = sync_timeout

	def _on_propagate(self):
		print("Propagating hypothesis")
		if self.sync_timeout is not None:
			self.msg_coordinator.request_stop(sync_timeout = self.sync_timeout)
		self.on_propagate()
		if self.sync_timeout is not None:
			self.msg_coordinator.request_proceed(sync_timeout = self.sync_timeout)

	def on_propagate(self):
		raise NotImplementedError
