from collections import OrderedDict

import tensorflow as tf

from reils.env.base import Space

INFERRED = True


class Frame(object):
	def __init__(self, keys, shapes, dtypes):
		self.shapes = shapes
		self.dtypes = dtypes
		self.keys = keys
		self.fields = None

	def get_feed_list(self):
		return list(self.fields.values())

	def __call__(self, **kwargs):
		if set(list(kwargs.keys())) != set(self.keys):
			raise AttributeError("Expected keys: " + self.keys)
		self.fields = OrderedDict(sorted(kwargs.items(), key=lambda t: t[0]))

	def __getitem__(self, item):
		return self.fields[item]

	def __len__(self):
		return len(self.fields)


# TODO Rename to FrameType
# TODO Support asynchronous frame creation by avoiding GILs
class FrameFactory(object):
	def __init__(self, **kwargs):
		kwargs = OrderedDict(sorted(list(kwargs.items()), key=lambda t: t[0]))

		pure_items = [item for item in kwargs.items() if not (len(item[1]) > 2 and item[1][2])]
		inferred_items = [item for item in kwargs.items() if (len(item[1]) > 2 and item[1][2])]
		all_items = pure_items + inferred_items

		all_keys_vals = list(map(list, zip(*all_items)))
		self.all_keys, all_vals = all_keys_vals
		all_shapes_dtypes = list(map(list, zip(*all_vals)))
		self.all_shapes, self.all_dtypes = all_shapes_dtypes[0], all_shapes_dtypes[1]

		self.keys, vals = list(map(list, zip(*pure_items)))
		shapes_dtypes = list(map(list, zip(*vals)))
		self.shapes, self.dtypes = shapes_dtypes[0], shapes_dtypes[1]

		self.all_key_to_indexes = dict((key, self.all_keys.index(key)) for key in self.all_keys)

	def new_instance(self, hide_inferred=True):
		keys = self.keys if hide_inferred else self.all_keys
		shapes = self.shapes if hide_inferred else self.all_shapes
		dtypes = self.dtypes if hide_inferred else self.all_dtypes
		return Frame(keys=keys, shapes=shapes, dtypes=dtypes)

	def __call__(self, **kwargs):
		frame = self.new_instance()
		frame(**kwargs)
		return frame

	def __len__(self):
		return len(self.keys)

	def __getitem__(self, key):
		index = self.all_key_to_indexes.get(key)
		assert index is not None, 'Unknown key'
		return index

def get_shape_dtype_tuple(space: Space):
	return space.shape, tf.as_dtype(space.dtype)

class ActorCriticFrameFactory(FrameFactory):
	def __init__(self, ob_space: Space, ac_space,
				 state=((), tf.float32),
				 reward=((), tf.float32),
				 start=((), tf.int64),
				 val_prediction=((), tf.float32),
				 advantage=((), tf.float32)):
		super(ActorCriticFrameFactory, self).__init__(
			state=state,
			ob=get_shape_dtype_tuple(ob_space),
			ac=get_shape_dtype_tuple(ac_space),
			rew=reward,
			start=start,
			vpred=val_prediction,
			adv=advantage
		)


class QFrameFactory(FrameFactory):
	def __init__(self, ob_space: Space, ac_space: Space,
				 state=((), tf.float32),
				 reward=((), tf.float32),
				 start=((), tf.int64)):
		super(QFrameFactory, self).__init__(
			ob=get_shape_dtype_tuple(ob_space),
			state=state,
			ac=get_shape_dtype_tuple(ac_space),
			rew=reward,
			start=start,
			next_ob=get_shape_dtype_tuple(ob_space),
			next_state=state
		)
