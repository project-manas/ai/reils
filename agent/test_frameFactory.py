from unittest import TestCase

import tensorflow as tf

from .frames import FrameFactory


class TestFrameFactory(TestCase):
	def test_new_instance(self):
		frame_factory = FrameFactory(b=[(2, 1, 128), tf.float32], c=[(1, 128), tf.float32], d=[(), tf.int8],
									 a=[(12), tf.float16])

		frame = frame_factory(c=2, d=1, a=4, b=3)
		frame1 = frame_factory(*frame.get_feed_list())
		frame2 = frame_factory.new_instance()
		frame2(a=4, b=3, d=1, c=2)
		if frame.shapes != frame1.shapes:
			self.fail()

		if frame.dtypes != frame1.dtypes:
			self.fail()

		if frame.get_feed_list() != frame1.get_feed_list():
			self.fail()

		if frame.shapes != frame2.shapes:
			self.fail()

		if frame.dtypes != frame2.dtypes:
			self.fail()

		if frame.get_feed_list() != frame2.get_feed_list():
			self.fail()

		for _ in range(100):
			if frame['a'] != 4 or frame1['b'] != 3:
				self.fail()
