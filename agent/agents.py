import tensorflow as tf

from sandblox import TFMethod
from .base import TFAgent
from ..hypothesis.action_selector import ActionSelector
from ..hypothesis.base import TFHypothesis


class MonoSelectorTFAgent(TFAgent):
	def __init__(self, sess, batch_shape, initial_state,
				 hypothesis: [None, TFHypothesis] = None, action_selectors: [None, [ActionSelector]] = None,
				 behave_op: [None, TFMethod] = None):
		super(MonoSelectorTFAgent, self).__init__(sess, batch_shape, initial_state,
												  hypothesis, action_selectors, behave_op)
		self.action_selection = tf.constant(0.0, name='mono_action_selection')

	def act(self, observation, action_selection=None):
		if action_selection is None:
			action_selection = self.action_selection
		return super(MonoSelectorTFAgent, self).act(observation, action_selection)


class StatelessTFAgent(TFAgent):
	# TODO StatelessTFAgent
	pass


class BlindTFAgent(TFAgent):
	# TODO BlindTFAgent
	pass


class StatelessMonoSelectorTFAgent(MonoSelectorTFAgent):
	def __init__(self, sess, hypothesis: [None, TFHypothesis], action_selectors: [None, [ActionSelector]] = None,
				 act_op: [None, TFMethod] = None):
		"""Agent that acts randomly by sampling actions from action space
		:param action_space: action space to sample from
		"""
		self.state = [0.]
		super(StatelessMonoSelectorTFAgent, self).__init__(sess, hypothesis, action_selectors, act_op)

	def act(self, observation, action_selection=None):
		if action_selection is None:
			action_selection = self.action_selection
		return super(StatelessMonoSelectorTFAgent, self).act(observation, action_selection)
