import tensorflow as tf

from reils.hypothesis.base import TFHypothesis
from sandblox import tf_method, TFMethod, Inp, Outs
from ..hypothesis.action_selector import ActionSelector


class Agent(object):
	@staticmethod
	def select_action(logits, action_selectors, action_selection):
		raise NotImplementedError

	def act(self, observation, action_selection):
		raise NotImplementedError

	def set_state(self, state):
		raise NotImplementedError

	def get_state(self, state):
		raise NotImplementedError


class TFAgent(Agent):
	def __init__(self, sess: tf.Session, batch_shape, initial_state,
				 hypothesis: TFHypothesis = None, action_selectors: [ActionSelector] = None,
				 behave_op: TFMethod = None):
		self.sess = sess
		self.batch_shape = batch_shape
		self.action_selectors = action_selectors
		self.hypothesis = hypothesis
		self.behave_op = behave_op
		self.state = initial_state

	@staticmethod
	def select_action(logits, action_selectors, action_selection):
		flattened_actions = tf.concat([action_selector(logits) for action_selector in action_selectors], 0)
		action_selection = tf.cast(action_selection, tf.int32, "action_selector_index")
		idxs = action_selection * len(action_selectors) + tf.range(0, action_selection.shape[0])
		return tf.gather(flattened_actions, idxs)

	class ActMethod(TFMethod):
		def eval(self, feed_dict=None, options=None, run_metadata=None, state=None):
			is_pythonic_state = not isinstance(self.cls.state, tf.Variable)

			if is_pythonic_state:
				if feed_dict is None:
					feed_dict = dict()
				if not self.i.curr_state in feed_dict:
					feed_dict[self.i.curr_state] = self.cls.state

			results = super(TFAgent.ActMethod, self).eval(feed_dict, options, run_metadata)

			if is_pythonic_state:
				self.cls.state = results[3]

			return results

	@tf_method(tf_method_class=ActMethod, override_inputs=True)
	def act(self, observation, action_selection):
		if self.behave_op is not None:
			assert observation == self.behave_op.i.observation
			behave_op = self.behave_op
		else:
			behave_op = self.hypothesis.behave(observation, self.hypothesis.new_state_placeholder([None]))

		logits, v_pred, next_state = behave_op.o
		curr_state = behave_op.i.curr_state
		action = self.select_action(logits, self.action_selectors, action_selection)

		return Inp(('observation', observation), ('action_selection', action_selection), ('curr_state', curr_state)), \
			   Outs(('action', action), ('logits', logits), ('v_pred', v_pred), ('next_state', next_state))

	def set_state(self, state):
		self.state = state

	def get_state(self):
		if isinstance(self.state, tf.Variable):
			return self.sess.run(self.state)
		else:
			return self.state
