import numpy as np
import tensorflow as tf

from datapipe.message_interface import MsgCoordinatorI, MessageInterfaceType
from datapipe.queue import HomoThreadHomoQueue, TfFIFOQueue
from reils.utils.analytics import Analytics
from .frames import FrameFactory
from ..utils import tf_util as U


class Explorer(MsgCoordinatorI, HomoThreadHomoQueue):
	def __init__(self, sess, frame_factory: FrameFactory,
				 q_size, n_threads: int, n_agents_per_thread: int = None, n_envs_per_thread: int = None,
				 scope_name: str = None, enable_rendering=False, analytics_enabled: bool = False,
				 enqueue_disabled: bool = False, *args, **kwargs):
		"""Drives the building of each agent's graph and running them in a multi-threaded environment
		Starts each agent on it's own thread with it's own queue into which it may enqueue frames.
		Exposes a dequeue_all op to dequeue these frames

		:param sess:
		:param frame_factory:
		:param q_size:
		:param n_agents_per_thread:
		:param on_build_graph:
		:param on_start
		"""
		self.enable_rendering = enable_rendering
		self.frame_factory = frame_factory
		self.agents_batch_size = n_agents_per_thread
		self.env_batch_size = n_envs_per_thread
		self.agent_batches = [None] * n_threads
		self.env_batches = [None] * n_threads
		self.analytics = Analytics(scope_name) if analytics_enabled else None
		self.enque_disabled = enqueue_disabled
		q_builder = kwargs.get('queue_builder',
							   lambda: TfFIFOQueue(self.frame_factory.shapes, self.frame_factory.dtypes,
												   q_size))
		super(Explorer, self).__init__(
			q_builder,
			n_threads,
			n_threads,
			sess,
			(self.on_build_graph, self.on_start),
			scope_name,
			*args
		)

	def dequeue_all(self):
		"""Perform a single dequeue of a single frame from each agents
		:return: list of frames corresponding to the result of a dequeue op from each agent frame queue
		"""
		with tf.variable_scope(self.scope.abs + '/'):
			return [queue.dequeue() for queue in self.queues]

	def on_build_graph(self, thread_index, thread_handler):
		"""
		Callback triggered when an agent/environment graph needs to be build.
		The produced Tensorflow operations are returned for use in multithreaded use
		:param thread_index: Index of the thread for whom the graph is being built
		:param thread_handler: Thread handler for coordination between the parent thread and the children threads.
		:return: session, enqueue_ops, my_envs, my_agents
		"""
		raise NotImplementedError

	def setup_thread(self, thread_index, thread_, envs, agents):
		pass

	def build_agents(self, thread_index, thread_handler, num: int):
		raise NotImplementedError

	def build_envs(self, thread_index, thread_handler, num: int):
		raise NotImplementedError

	def on_start(self, thread_index, thread_handler, *args):
		"""
		Thread execution
		:param thread_index:
		:param thread_handler:
		:param args:
		:return: Does not return unless requested by coordinator or exception is raised
		"""
		raise NotImplementedError


class OneOneExplorer(Explorer):
	def __init__(self, sess, frame_factory: FrameFactory,
				 q_size, n_threads: int, n_agents_per_thread=1, n_envs_per_thread=1,
				 scope_name: str = None,
				 enable_rendering=False, analytics_enabled: bool = False, enqueue_disabled: bool = False,
				 *args):
		super(OneOneExplorer, self).__init__(sess, frame_factory,
											 q_size, n_threads, n_agents_per_thread, n_envs_per_thread,
											 scope_name,
											 enable_rendering, analytics_enabled, enqueue_disabled,
											 *args)

	def on_build_graph(self, thread_index, thread_handler):
		"""
		Callback triggered when an agent/environment graph needs to be build.
		The produced Tensorflow operations are returned for use in multithreaded use
		:param thread_index: Index of the thread for whom the graph is being built
		:param thread_handler: Thread handler for coordination between the parent thread and the children threads.
		:return:
		"""
		queue = self.queues[thread_index]
		sess = self.sess

		my_envs = []
		batch_size = len(self.env_batches) / self.thread_count + 1 if thread_index < len(
			self.env_batches) % self.thread_count else 0
		my_agents = self.build_agents(thread_index, thread_handler, batch_size)
		for i in range(thread_index, len(self.env_batches), self.thread_count):
			env = self.build_envs(i, thread_handler)
			my_envs.append(env)
			self.env_batches[i] = env

		with sess.graph.as_default():
			with tf.variable_scope('agent_' + str(thread_index)):
				tuple_of_stacks = [tf.placeholder(dtype, [len(my_envs)] + shape.dims, key)
								   for shape, dtype, key in
								   zip(queue.shapes, queue.dtypes, self.frame_factory.keys)]
				enqueues = U.function(tuple_of_stacks,
									  queue.enqueue_many(tuple_of_stacks))
			return sess, enqueues, my_envs, my_agents

	def on_start(self, thread_index, thread_handler, *args):
		print("Agent thread %d: Starting" % thread_index)
		sess, enqueues, my_envs, my_agents = args

		self.setup_thread(thread_index, thread_handler, my_envs, my_agents)
		total_reward = np.zeros(len(my_agents), np.float32)
		message_interface = MessageInterfaceType(thread_handler.message_interface)

		# Local refs to avoid GIL on self
		next_frame = self.next_frame
		enable_rendering = self.enable_rendering
		analytics_enabled = self.analytics is not None
		analytics = self.analytics
		perform_enqueue = not self.enque_disabled

		# TODO Support non-vector API when number of threads = number of agents
		obs = [env.reset() for env in my_envs]
		with sess.as_default():
			while not thread_handler.should_stop():

				if message_interface.has_message():
					print("Thread %d: Awaiting hypothesis sync" % thread_index)
					message_interface.barrier(10)
					print("Thread %d: Received sync completion signal" % thread_index)

				curr_states = [agent.state for agent in my_agents]
				multiframe, next_obs = next_frame(thread_index, thread_handler, my_envs, my_agents, obs, curr_states)

				if analytics_enabled:
					analytics.tick(len(next_obs))

				total_reward += multiframe['rew']
				is_ep_starts = multiframe['start']

				total_reward *= 1 - is_ep_starts
				obs = [my_envs[agent_index].reset() if is_ep_starts[agent_index] else next_obs[agent_index] for
					   agent_index in range(len(my_agents))]

				if thread_handler.should_stop():
					break
				try:
					if perform_enqueue:
						enqueues(*multiframe.get_feed_list())
				except tf.errors.CancelledError as e:
					if not thread_handler.should_stop():
						raise e
				if thread_index == 0 and enable_rendering:
					my_envs[0].render()
			for env in my_envs:
				env.close()
			print("Thread %d: Exiting on coordinator request" % thread_index)

	def build_agents(self, thread_index, thread_handler, num: int):
		raise NotImplementedError

	def build_envs(self, thread_index, thread_handler, num: int):
		raise NotImplementedError

	# TODO Remove curr_state as argument since it can be accessed from the agent
	@staticmethod
	def next_frame(self, thread_index, env, agent, ob, curr_state):
		raise NotImplementedError


# TODO Support case where number of agents != number of environments
class BatchAgentExplorer(Explorer):

	def __init__(self, sess, frame_factory: FrameFactory,
				 q_size, n_threads: int, n_agents_per_thread=1, n_envs_per_thread=1,
				 scope_name: str = None,
				 enable_rendering=False, analytics_enabled: bool = False, enqueue_disabled: bool = False,
				 *args, **kwargs):
		super(BatchAgentExplorer, self).__init__(sess, frame_factory,
												 q_size, n_threads, n_agents_per_thread, n_envs_per_thread,
												 scope_name,
												 enable_rendering, analytics_enabled, enqueue_disabled,
												 *args, **kwargs)

	def on_build_graph(self, thread_index, thread_handler):
		"""
		Callback triggered when an agent/environment graph needs to be build.
		The produced Tensorflow operations are returned for multithreaded use
		:param thread_index: Index of the thread for whom the graph is being built
		:param thread_handler: Thread handler for coordination between the parent thread and the children threads.
		:return:
		"""
		queue = self.queues[thread_index]
		sess = self.sess

		self.agent_batches[thread_index] = self.build_agents(thread_index, thread_handler, self.agents_batch_size)
		self.env_batches[thread_index] = self.build_envs(thread_index, thread_handler, self.agents_batch_size)

		# with sess.graph.as_default():
		# 	with tf.variable_scope('agent_batch_%d' % thread_index):
		# 		tuple_of_stacks = [tf.placeholder(dtype, [self.env_batch_size] + shape.dims, key)
		# 						   for shape, dtype, key in
		# 						   zip(queue.shapes, queue.dtypes, self.frame_factory.keys)]
		# 		enqueues = U.function(tuple_of_stacks, queue.enqueue_many(tuple_of_stacks))
		# 	return enqueues
		return queue.enqueue(None)

	def on_start(self, thread_index, thread_handler, *args):
		print("Agent thread %d: Starting" % thread_index)
		enqueues, = args
		sess, env, agent = self.sess, self.env_batches[thread_index], self.agent_batches[thread_index]
		self.setup_thread(thread_index, thread_handler, env, agent)

		message_interface = MessageInterfaceType(thread_handler.message_interface)

		# Local refs to avoid GIL on self
		next_frame = self.next_frame
		enable_rendering = self.enable_rendering
		analytics_enabled = self.analytics is not None
		analytics = self.analytics
		perform_enqueue = not self.enque_disabled
		agents_batch_size = self.agents_batch_size

		curr_state = agent.get_state()

		# TODO Support non-vector API when number of threads = number of agents
		obs = env.reset()
		with sess.as_default():
			while not thread_handler.should_stop():

				if message_interface.has_message():
					print("Thread %d: Awaiting hypothesis sync" % thread_index)
					message_interface.barrier(10)
					print("Thread %d: Received sync completion signal" % thread_index)

				multiframe, next_ob = next_frame(thread_index, thread_handler, env, agent, obs, curr_state)
				curr_state = multiframe['state']

				if analytics_enabled:
					analytics.tick(len(next_ob))

				is_ep_start = multiframe['start']
				obs = env.conditional_reset(is_ep_start, next_ob)
				try:
					if perform_enqueue:
						enqueues(multiframe.get_feed_list())
				except tf.errors.CancelledError as e:
					if not thread_handler.should_stop():
						raise e
				if thread_index == 0 and enable_rendering:
					env.render()
				if thread_handler.should_stop():
					break
			env.close()
			print("Thread %d: Exiting on coordinator request" % thread_index)

	def build_agents(self, thread_index, thread_handler, num: int):
		raise NotImplementedError

	def build_envs(self, thread_index, thread_handler, num: int):
		raise NotImplementedError

	# TODO Remove curr_state as argument since it can be accessed from the agent
	@staticmethod
	def next_frame(self, thread_index, env, agent, ob, curr_state):
		raise NotImplementedError
