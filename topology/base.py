import numpy as np
import traceback
from queue import Empty
from typing import NewType

import tensorflow as tf

from reils import Analytics
from reils.utils.logger import Logger
from ..agent.explorer import BatchAgentExplorer
from ..agent.frames import FrameFactory
from ..agent.hypothesis_propagator import HypothesisPropagator
from ..experience.base import Experience
from ..experience.samplers import Sampler
from ..feeder.base import Feeder
from ..trainer.base import TFTrainer

ExperienceSamplerClass = NewType('ExperienceSamplerClass', Sampler)


class Topology(object):
	def __init__(self, sess, frame_factory: FrameFactory, batch_size,
				 log_dir: str = None, verbosity: int = 3):
		self.verbosity = verbosity
		self.sess = sess
		self.frame_factory = frame_factory
		self.batch_size = batch_size

		self.explorer = self.dataset = self.experience = self.sampler = self.feed_pipe = self.trainer = self.hypothesis = self.hypothesis_propagator = None
		self.log_dir = log_dir
		if self.log_dir:
			self.logger = Logger(self.log_dir + '/train')
			self.train_saver = None
		self.q_size_ops = None

		self.explorer_dequeue = None

	def get_experience_dataset(self):
		if self.dataset is None:
			self.dataset = tf.data.Dataset().from_generator(
				self.experience_generator,
				tuple(self.frame_factory.dtypes),
				tuple([(self.batch_size,) + shape for shape in self.frame_factory.shapes]))
		return self.dataset

	def on_setup_topology(self):
		raise NotImplementedError

	def build_graph(self):
		self.explorer, self.experience, self.sampler, self.dataset, self.trainer, self.hypothesis_propagator = self.on_setup_topology()
		self.explorer_dequeue = self.explorer.dequeue_all()
		self.sess.run(self.iterator.initializer)
		self.explorer.build_unbuilt_graphs()
		assert self.trainer.graph.is_built()
		if self.log_dir is not None:
			tf.summary.FileWriter(self.log_dir + '/graph', self.sess.graph)
		self.train_saver = tf.train.Saver()

	def construct_trajectory_pipe(self) -> BatchAgentExplorer:
		raise NotImplementedError

	def construct_experience(self) -> Experience:
		raise NotImplementedError

	def construct_sampler(self, experience: Experience) -> Sampler:
		raise NotImplementedError

	def construct_feed_pipe(self, sampler: Sampler) -> Feeder:
		raise NotImplementedError

	def construct_trainer(self, feed_pipe: Feeder) -> TFTrainer:
		raise NotImplementedError

	def construct_hypothesis_propagator(self) -> HypothesisPropagator:
		raise NotImplementedError

	def start(self):
		print("Starting Topology")
		with self.sess.graph.as_default():
			self.build_graph()
		with self.sess.as_default():
			self.sess.run(tf.global_variables_initializer())

			try:
				print('Starting exploration')
				self.explorer.start()
				gen = self.experience_generator()
				next(gen)

				print('Starting training')
				self.on_training()
			except Exception as e:
				traceback.print_exc()
				print('Training failed! Stopping and joining on auxiliary worker threads such as experience collection')
			finally:
				self.stop()
		print("Topo complete")

	def on_training(self, *args):
		raise NotImplementedError

	# TODO Create dedicated interface for experience generator to avoid GIL on topology instance
	def experience_generator(self):
		# TODO Condition on stop signal from main
		analytics = Analytics('topology')
		batch_size = self.batch_size
		sess = self.sess
		deq = self.explorer_dequeue
		sampler = self.sampler
		frame_batch = []
		repeat_factor = batch_size / 512
		partial_batch_size = int(batch_size / repeat_factor)
		rep = lambda arr: np.repeat(arr, repeat_factor, 0)
		try:
			while True:
				analytics.tick(batch_size * len(deq))
				for d in deq:
					yield tuple(rep(y) for y in d())
				# continue
				# # for sample in self.sampler:
				# # 	multiframe = tuple(zip(*sample))
				# # 	yield multiframe
				#
				# while len(frame_batch) < partial_batch_size:
				# 	frame_batch.extend([d() for d in deq])
				# 	if len(frame_batch) % 10000 == 0:
				# 		print('frame batch %d' % len(frame_batch))
				# analytics.tick(partial_batch_size)
				# # print('%d original frames' % len(frame_batch))
				# partial_batch = frame_batch[:int(batch_size / repeat_factor)]
				# repeated = []
				# # print('%d partial batch' % len(partial_batch))
				# for _ in range(repeat_factor):
				# 	repeated.extend(partial_batch)
				# # print('multiframe of %d frames (with repetition)' % len(repeated))
				# multiframe = tuple(zip(*repeated))
				# yield multiframe
				# frame_batch = frame_batch[int(batch_size / repeat_factor):]
		except tf.errors.OutOfRangeError or tf.errors.CancelledError or Empty as e:
			raise Exception(
				'Stopping experience collection: Trajectory pipe being closed and out of consumable frames.', e)

	def stop(self):
		self.explorer.stop_all()
