import traceback

from ..experience.base import StackedExperience, FrameT, TupledExperience


class StackedBatchExperience(StackedExperience):
	"""
	Underlying data structure consists of a list of batches of tuples for efficient per batch of frame accessing.
	"""

	@property
	def tupled(self) -> [list, tuple]:
		return ([[frame.fields[key] for frame in batch] for batch in self.frames] for key in self.frames[0][0].keys)


class TupledBatchExperience(TupledExperience):
	"""
	Underlying data structure consists of a list of batches of tuples for efficient per batch of frame accessing.
	"""

	def add(self, batch: [FrameT]):
		for key in self.keys:
			self.frames[key].append([frame.fields[key] for frame in batch])
		try:
			self.block_condition.notify(1)
			print('Successfully notified')
		except RuntimeError as e:
			if 'un-acquired' not in str(e):
				traceback.print_exc()
			else:
				print(e)
				print(self.block_condition)

	def __getitem__(self, item):
		# TODO Support getting a batch of stacks from TupledBatchExperience
		raise NotImplementedError

	@property
	def tupled(self) -> [list, tuple]:
		return self.frames


# TODO Defined CircularTupledExperience
class CircularStackedExperience(StackedExperience):
	"""
	Extends StackedExperience to provide circular experience.
	"""

	def __init__(self, capacity: int, keys):
		assert capacity != 0
		self.cap = capacity
		self.back = 0
		super(CircularStackedExperience, self).__init__(keys)

	def add(self, frame: FrameT):
		super(CircularStackedExperience, self).add(frame)
		if len(self.frames) == self.cap:
			self.back = 0
			self.add = self._overwrite_first_in

	def _overwrite_first_in(self, frame: FrameT):
		self.frames[self.back] = frame
		self.back = (self.back + 1) % self.cap
		try:
			self.block_condition.notify(1)
		except RuntimeError as e:
			if 'un-acquired' not in str(e):
				traceback.print_exc()
			else:
				print(e)


class CircularTupledExperience(TupledExperience):
	"""
	Extends StackedBatchExperience to provide circular batch experience.
	"""

	def __init__(self, capacity: int, keys):
		assert capacity != 0
		self.cap = capacity
		self.back = 0
		super(CircularTupledExperience, self).__init__(keys)

	def add(self, frame: FrameT):
		super(TupledExperience, self).add(frame)
		if len(self) == self.cap:
			self.back = 0
			self.add = self._overwrite_first_in

	def _overwrite_first_in(self, frame: [FrameT]):
		for key in self.keys:
			self.frames[key][self.back] = frame.fields[key]
		try:
			self.block_condition.notify(1)
		except RuntimeError:
			pass
		self.back = (self.back + 1) % self.cap


# TODO Use multiple inheritance of CircularStackedExperience and StackedBatchExperience instead
class CircularStackedBatchExperience(StackedBatchExperience):
	"""
	Extends StackedBatchExperience to provide circular batch experience.
	"""

	def __init__(self, capacity: int, keys):
		assert capacity != 0
		self.cap = capacity
		self.back = 0
		super(CircularStackedBatchExperience, self).__init__(keys)

	def add(self, frame: FrameT):
		super(CircularStackedBatchExperience, self).add(frame)
		if len(self.frames) == self.cap:
			self.back = 0
			self.add = self._overwrite_first_in

	def _overwrite_first_in(self, frame: FrameT):
		self.frames[self.back] = frame
		self.back = (self.back + 1) % self.cap


class CircularTupledBatchExperience(TupledBatchExperience):
	"""
	Extends StackedBatchExperience to provide circular batch experience.
	"""

	def __getitem__(self, item):
		# TODO Support getting a batch of stacks from CircularTupledBatchExperience
		raise RuntimeError('Not implemented')

	def __init__(self, capacity: int, keys):
		assert capacity != 0
		self.cap = capacity
		self.back = 0
		super(CircularTupledBatchExperience, self).__init__(keys)

	def add(self, frame: FrameT):
		super(CircularTupledBatchExperience, self).add(frame)
		if len(self) == self.cap:
			self.back = 0
			self.add = self._overwrite_first_in

	def _overwrite_first_in(self, batch: [FrameT]):
		for key in self.keys:
			self.frames[key][self.back] = [frame.fields[key] for frame in batch]
		try:
			self.block_condition.notify(1)
		except RuntimeError:
			pass
		self.back = (self.back + 1) % self.cap
