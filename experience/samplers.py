import time
from collections import Iterable

import numpy as np
from numpy.random.mtrand import RandomState

from ..experience import Experience, TupledExperience


class Sampler(object):
	def __init__(self, experience: Experience, block_on_empty: bool = True, next_timeout: [None, float] = None):
		self.experience = experience
		self.block_on_empty = block_on_empty
		self.next_timeout = next_timeout
		self.tupled_next_enabled = False
		self.sample_size = None  # TODO Why is this attribute here?!

	def use_tupled_next(self, tupled_next: bool):
		self.tupled_next_enabled = tupled_next

	def __iter__(self):
		return self

	def update_index(self):
		raise NotImplementedError

	def access_stack(self, stack):
		raise NotImplementedError

	def control_access(self):
		if len(self.experience) == 0:
			if self.block_on_empty:
				with self.experience.block_condition:
					print('acquired lock ' + str(self.experience.block_condition))
					self.experience.block_condition.wait(self.next_timeout)
					if len(self.experience) == 0:
						raise TimeoutError("Timeout waiting for experience to fill")
			else:
				raise StopIteration('experience is empty')

	def __next__(self):
		"""
		Examples
		---
		Test non-blocking __next__ with empty sampler:
		>>> try:
		...	test_non_blocking_next(CircularSampler)
		... except StopIteration as e:
		...	e
		StopIteration('experience is empty',)

		Test blocking __next__ timeout with empty sampler:
		>>> try:
		...	test_blocking_next(CircularSampler)
		... except TimeoutError as e:
		...	e
		TimeoutError('Timeout waiting for experience to fill',)
		>>> experience = Experience(); ignore_return = [experience.add(i) for i in range(5)]; sampler = RandomSampler(experience, seed=1234); [sampler.__next__() for _ in range(2)]
		[4, 5]
		"""
		self.control_access()
		self.update_index()
		if self.tupled_next_enabled:
			if isinstance(self.experience, TupledExperience):
				return (self.access_stack(self.experience.tupled[key]) for key in self.experience.keys)
			else:
				return (self.access_stack(tup) for tup in self.experience.tupled)
		else:
			return self.access_stack(self.experience)


def test_non_blocking_next(sampler_class, **kwargs):
	"""
	Test non-blocking __next__ with empty sampler
	:raises StopIteration
	"""
	experience = Experience()
	sampler = sampler_class(experience, **kwargs, block_on_empty=False)
	[sampler.__next__() for _ in range(2)]


def test_blocking_next(sampler_class, **kwargs):
	"""
	Test blocking __next__ with empty sampler
	:raises TimeoutIteration
	"""
	experience = Experience()
	sampler = sampler_class(experience, **kwargs, block_on_empty=True, next_timeout=0.0)
	[sampler.__next__() for _ in range(2)]


class SequentialSampler(Sampler):
	def __init__(self, experience: Experience, block_on_empty: bool = True, next_timeout: [None, float] = None):
		super(SequentialSampler, self).__init__(experience, block_on_empty, next_timeout)
		self.index = -1

	def update_index(self):
		self.index = self.index + 1
		if self.index >= len(self.experience):
			raise StopIteration

	def access_stack(self, stack):
		return stack[self.index]


class CircularSampler(SequentialSampler):
	def __init__(self, experience: Experience,
				 block_on_empty: bool = True, next_timeout: [None, float] = 1.0):
		super(CircularSampler, self).__init__(experience, block_on_empty, next_timeout)
		self.index = -1

	def update_index(self):
		self.index = (self.index + 1) % len(self.experience)


class RandomSampler(SequentialSampler):
	def __init__(self, experience, state: [None, RandomState] = None, seed: [None, int, np.ndarray, Iterable] = None,
				 block_on_empty: bool = True, next_timeout: [None, float] = 1.0):
		super(RandomSampler, self).__init__(experience, block_on_empty, next_timeout)
		if state and seed:
			raise ValueError("Cannot simultaneously provide state and seed")
		if state:
			self.state = state
		else:
			self.state = np.random.RandomState(seed)
		self.index = 0

	def update_index(self):
		self.index = self.state.randint(0, len(self.experience))


class SequentialManySampler(SequentialSampler):
	def __init__(self, experience, sample_size: int, block_on_empty: bool = True, next_timeout: [None, float] = 1.0):
		super(SequentialManySampler, self).__init__(experience, block_on_empty, next_timeout)
		self.sample_size = sample_size

	def access_stack(self, stack):
		if self.index < len(self.experience):
			return stack[self.index - self.sample_size:self.index]
		else:
			raise StopIteration

	def update_index(self):
		self.index = self.index + self.sample_size

	def control_access(self):
		deadline = time.time() + self.next_timeout
		if self.block_on_empty:
			with self.experience.block_condition:
				while len(self.experience) < self.sample_size:
					time_left = deadline - time.time()
					if time_left <= 0:
						raise TimeoutError("Timeout waiting for experience to fill")
					self.experience.block_condition.wait(time_left)
		elif len(self.experience) < self.sample_size:
			raise StopIteration('experience is empty')


class CircularSequentialManySampler(SequentialManySampler):
	def access_stack(self, stack):
		if self.index < self.sample_size:
			end = len(stack) + self.index
		else:
			end = self.index
		prev_index = end - self.sample_size
		if end <= len(stack):
			return stack[prev_index:end]
		else:
			return stack[prev_index:] + stack[:end - len(stack)]

	def update_index(self):
		# self.index = (self.index + self.sample_size) % len(self.experience)
		self.index = self.experience.back
