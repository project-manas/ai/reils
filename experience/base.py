import threading
from typing import TypeVar, Generic

FrameT = TypeVar('FrameT')


class Experience(Generic[FrameT]):
	def __init__(self, keys):
		self.keys = keys
		self.frames_size = len(keys)
		self.frames = None
		self.block_condition = threading.Condition()

	def add(self, frame: FrameT):
		try:
			self.block_condition.notify(1)
		except RuntimeError:
			pass

	def __len__(self):
		raise NotImplementedError

	def __getitem__(self, index):
		raise NotImplementedError

	@property
	def tupled(self):
		if self.frames_size is None:
			raise AssertionError(
				"Stacks not supported for jagged experiences. Provide frame_size to assure no jaggedness")
		raise NotImplementedError

	def reset(self):
		raise NotImplementedError


# TODO Work on the 2 children's names and add test cases for each

class StackedExperience(Experience):
	"""
	Underlying data structure consists of a list of tuples for efficient per frame accessing.
	Each frame corresponds to a frame
	"""

	def __init__(self, keys):
		super(StackedExperience, self).__init__(keys)
		self.frames = []

	def add(self, frame: FrameT):
		self.frames.append(frame)
		super(StackedExperience, self).add(frame)

	def __len__(self):
		return len(self.frames)

	def __getitem__(self, index) -> FrameT:
		return self.frames[index]

	@property
	def tupled(self) -> [list, tuple]:
		return ([frame.fields[key] for frame in self.frames] for key in self.keys)

	def reset(self):
		self.frames = []


class TupledExperience(Experience):
	"""
	Underlying data structure consists of a tuple of lists.
	Each element of the tuple represents a list of all of the corresponding inserted frames.
	Especially efficient for vector operations performed on an attribute of the frame.
	"""

	def __init__(self, keys):
		super(TupledExperience, self).__init__(keys)
		self.frames = {key: [] for key in self.keys}

	def add(self, frame: FrameT):
		for tuple_element, tuple_index in enumerate(frame):
			self.frames[tuple_index].append(tuple_element)
		super(TupledExperience, self).add(frame)

	def __len__(self):
		return len(self.frames[self.keys[0]])  # assumes len of all frame stacks are equal

	def __getitem__(self, index):
		return FrameT(frame[index] for frame in self.frames)

	@property
	def tupled(self) -> [list, tuple]:
		return self.frames

	def reset(self):
		self.frames = {}
