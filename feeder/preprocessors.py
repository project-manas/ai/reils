import numpy as np


def gae(sampled_values, frame_factory, batch_size, *args, **kwargs):
	np_vals = [arr.transpose([1, 0] + list(range(arr.ndim)[2:])) for arr in
			   [np.array(stack) for stack in sampled_values]]

	gamma = kwargs['gam']
	lam = kwargs['lam']

	# Pass by reference
	ad = np_vals[frame_factory['adv']]
	re = np_vals[frame_factory['rew']]
	st = np_vals[frame_factory['start']]
	vp = np_vals[frame_factory['vpred']]

	time_len = np_vals[0].shape[1] - 1

	last_adv = np.zeros(batch_size)
	ad[:, time_len] = 0
	for t in reversed(range(time_len)):
		last_adv *= 1 - st[:, t + 1]  # last_adv = 0 if start(t+1) == 1 else last_adv
		non_terminal = 1 - st[:, t]
		# td_error = r + lam*v(t+1) - v(t)
		td_error = re[:, t] + lam * vp[:, t + 1] * non_terminal - vp[:, t]
		ad[:, t] = last_adv = td_error + gamma * lam * non_terminal * last_adv
	# noinspection PyUnusedLocal
	re = ad + vp

	# randidx = np.arange(0, time_len + 1)
	# np.random.shuffle(randidx)
	#
	# for el in range(len(frame_factory)):
	# 	for b in range(batch_size):
	# 		np_vals[el][b] = np_vals[el][b][randidx]

	return np_vals


def tdzero(sampled_values, frame_factory, batch_size, *args, **kwargs):
	np_vals = [arr.transpose([1, 0] + list(range(2, len(arr.shape)))) for arr in
			   [np.array(stack) for stack in sampled_values]]

	gamma = kwargs['gam']
	lam = kwargs['lam']

	ad = frame_factory['adv']
	re = frame_factory['rew']
	st = frame_factory['start']
	vp = frame_factory['vpred']

	time_len = np_vals[0].shape[1] - 1

	for b in range(batch_size):
		last_adv = 0
		np_vals[ad][b][time_len] = 0
		for t in reversed(range(time_len)):
			if np_vals[st][b][t + 1]:
				last_adv = 0
			non_terminal = 1 - np_vals[st][b][t]
			delta = np_vals[re][b][t] + gamma * np_vals[vp][b][t + 1] * non_terminal - np_vals[vp][b][t]
			np_vals[ad][b][t] = last_adv = delta + gamma * lam * non_terminal * last_adv
		np_vals[re][b] = np_vals[ad][b] + np_vals[vp][b]

	return np_vals


def qtargets(sampled_values, frame_factory, batch_size, *args, **kwargs):
	np_vals = [np.array(arr) for arr in sampled_values]
	return np_vals
