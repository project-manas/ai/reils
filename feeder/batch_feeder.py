from typing import Type

import tensorflow as tf

from datapipe.queue import HomoThreadMonoQueue, Queue
from reils.experience.samplers import Sampler
from ..agent.frames import FrameFactory
from ..utils import tf_util as U


class BatchFeeder(HomoThreadMonoQueue):
	def __init__(self,
				 queue_class: Type[Queue],
				 sess: tf.Session,
				 frame_factory: FrameFactory,
				 sampler: Sampler,
				 batch_size: int,
				 preprocessor,
				 scope_name: str = None, *args, **kwargs):
		self.frame_factory = frame_factory
		self.preprocessor = preprocessor
		group_shape = (batch_size,)
		if sampler.sample_size is not None:
			group_shape = group_shape + (sampler.sample_size,)

		dtypes = [dtype for dtype in frame_factory.all_dtypes]
		shapes = [group_shape + shape for shape in frame_factory.all_shapes]

		self.setup_scope(scope_name)
		with tf.variable_scope(self.scope.rel, reuse=True):
			queue = queue_class(dtypes=dtypes, shapes=shapes, capacity=5, shared_name=self.scope.rel,
								scope_name=self.scope.rel)

		super(BatchFeeder, self).__init__(
			queue, 1, sess,
			(self.on_build, self.on_start),
			scope_name,
			frame_factory, sampler, batch_size, *args, **kwargs)

	def build_unbuilt_graphs(self):
		super(BatchFeeder, self).build_unbuilt_graphs()

	def dequeue(self):
		with tf.variable_scope(self.scope.abs + '/'):
			return self.mono_queue.dequeue()

	def get_graph(self):
		return tf.get_default_graph()

	def on_build(self, thread_index, thread_handler, frame_factory, sampler, batch_size, *args, **kwargs):
		placeholders = []
		mono_q = self.mono_queue
		for dtype, shape, name in zip(mono_q.dtypes, mono_q.shapes, frame_factory.keys):
			placeholders.append(tf.placeholder(dtype, shape, name))

		enqueue_op = U.function(placeholders, mono_q.enqueue(placeholders))
		return self.sess, enqueue_op, frame_factory, sampler, batch_size, args, kwargs

	def on_start(self,
				 thread_index,
				 thread_handler,
				 sess: tf.Session,
				 enqueue,
				 frame_factory: FrameFactory,
				 sampler: Sampler,
				 batch_size: int,
				 *args,
				 **kwargs):
		print("Feeder is starting")

		with sess.as_default():
			while not thread_handler.should_stop():
				try:
					vals_tuple = list(sampler.__next__())
					preprocessed_vals = self.preprocessor(vals_tuple, self.frame_factory, batch_size, *args, **kwargs)
					try:
						enqueue(*preprocessed_vals)

					except ValueError as e:
						print(e)

				except tf.errors.CancelledError as e:
					if not thread_handler.should_stop():
						raise e

			print("Stopping Feeder")
