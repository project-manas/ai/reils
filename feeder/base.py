import time

import numpy as np
import tensorflow as tf

from datapipe.queue import Queue, HomoThreadMonoQueue
from ..agent.frames import FrameFactory
from ..experience.samplers import Sampler
from ..utils import tf_util as U


class Feeder(HomoThreadMonoQueue):
	def __init__(self, frame_factory: FrameFactory, sampler: Sampler, queue_class, batch_size,
				 scope_name: str = None):
		sess = tf.get_default_session()
		self.frame_factory = frame_factory
		queue = queue_class(
			dtypes=frame_factory.dtypes, shapes=frame_factory.shapes, capacity=2 * batch_size)

		super(Feeder, self).__init__(
			queue, 1, sess,
			(self.on_build, self.on_start),
			scope_name,
			sess, sampler)

		self.batch_size = batch_size
		with tf.variable_scope(self.scope.abs):
			self.dequeue_op = self.mono_queue.dequeue()

	def dequeue(self):
		return self.dequeue_op

	@staticmethod
	def on_build(thread_index, thread_handler, queue: HomoThreadMonoQueue, sess: tf.Session,
				 sampler: Sampler):
		placeholders = []
		single_queue = queue[0]
		for dtype, shape in zip(single_queue.dtypes, single_queue.shapes):
			placeholders.append(tf.placeholder(dtype, shape))
		enqueue_or_many_op = single_queue.enqueue(placeholders)
		return sess, queue, U.function(placeholders, enqueue_or_many_op), sampler

	@staticmethod
	def on_start(thread_index,
				 thread_handler,
				 sess: tf.Session,
				 queue: Queue,
				 enqueue,
				 sampler: Sampler):
		with sess.as_default():
			while not thread_handler.should_stop():
				time.sleep(np.random.uniform(0.0, 0.01))
				try:
					q_size = sess.run(queue.size())
					print("GPU Queue -- size %d: %s" % (q_size, "Enqueue"))
					values = sampler.__next__()
					enqueue(*values)
				except tf.errors.CancelledError as e:
					if not thread_handler.should_stop():
						raise e
