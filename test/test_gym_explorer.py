import logging
import random
import threading
import unittest
from time import sleep
from unittest import TestCase

import gym
import numpy as np
import tensorflow as tf

from datapipe import utils as U
from datapipe.message_interface import MessageInterfaceType, MsgCoordinatorI
from reils import FrameFactory
from reils.hypothesis.action_selector import Stochastic
from reils.hypothesis.base import TFHypothesis
from reils.utils.distributions import make_pdtype
from reils_gym import GymEnvironment
from reils_gym.sample.assault import AssaultExplorer
from ..agent.agents import StatelessMonoSelectorTFAgent
from ..agent.explorer import BatchAgentExplorer
from ..agent.hypothesis_propagator import CoordinatedPropagator
from ..env.base import Environment
from ..experience.base import Experience
from ..experience.experience_buffers import CircularStackedExperience
from ..experience.samplers import CircularSequentialManySampler
from ..hypothesis.policies import BiasedRandomTFHypothesis


class CoordinatedRandomPropagator(CoordinatedPropagator):
	def __init__(self, sess, action_space, shared_hypothesis: TFHypothesis, msg_coordinator: MsgCoordinatorI):
		super(CoordinatedRandomPropagator, self).__init__(msg_coordinator, False, True)
		self.sess = sess
		with self.sess.graph.as_default():
			source_hypothesis = BiasedRandomTFHypothesis(sess, action_space)
			self.assign_trainable_vars = shared_hypothesis.assign_trainable_vars(source_hypothesis)

	def on_start_propagation(self):
		with self.sess.as_default():
			super(CoordinatedRandomPropagator, self).on_start_propagation()
		print('Stopping propagation')

	def on_propagate(self):
		sleep(0.1)
		print("Propagating hypothesis")
		self.msg_coordinator.request_stop(sync_timeout=2)
		self.sess.run(self.assign_trainable_vars)
		self.msg_coordinator.request_proceed(sync_timeout=2)


class AssaultBlindExplorer(AssaultExplorer):
	"""
	Coordinates the interactions between agents and their environments.
	Supports any mapping between agent and environment.
	"""

	def __init__(self, sess, frame_factory: FrameFactory, q_size, n_agents_per_thread, shared_hypothesis, act_op,
				 name: str = None):
		super(AssaultExplorer, self).__init__(sess, frame_factory, q_size, n_agents_per_thread, 'Assault-v0', name)
		self.act_op = act_op
		self.shared_hypothesis = shared_hypothesis

	def build_agents(self, thread_index, thread_handler):
		return StatelessMonoSelectorTFAgent(self.sess, self.shared_hypothesis, act_op=self.act_op)

	def next_frame(self, envir, agnt, ob, curr_state):
		sleep(0.5)
		action, _, value_pred, _ = agnt.act.eval(
			feed_dict={agnt.act.i.observation: np.expand_dims(np.expand_dims(ob, 0), 0)})
		next_ob, reward, is_ep_start, _ = envir.step(action[0])
		return self.frame_factory(state=curr_state, ac=action[0], adv=0, ob=ob, rew=reward, vpred=value_pred[0],
								  start=int(is_ep_start))


class TestSession(object):
	def __init__(self):
		self.sess = tf.Session()
		self.ob_space, self.action_space = GymEnvironment.get_spaces('Assault-v0')
		self.frame_factory = FrameFactory(
			state=((), tf.float32),
			ac=(self.action_space.shape, self.action_space.dtype),
			ob=(self.ob_space.shape, self.ob_space.dtype),
			rew=((), tf.float32),
			val_pred=((), tf.float32),
			start=((), tf.float32)
		)

		with self.sess.graph.as_default():
			with tf.variable_scope('shared_hypothesis', reuse=None):
				ob = tf.placeholder(name="ob", dtype=tf.float32, shape=[None, None] + list(self.ob_space.shape))
				self.shared_hypothesis = BiasedRandomTFHypothesis(self.sess, self.action_space)
				self.shared_hypothesis.behave(ob).freeze()
				agent = StatelessMonoSelectorTFAgent(self.sess, self.shared_hypothesis)
				self.act_op = agent.act(ob)

		act_op = None
		self.multiAgentPipe = AssaultExplorer(self.sess, self.frame_factory, 16, 4, self.shared_hypothesis, act_op)

		with self.sess.as_default():
			self.multiAgentPipe.build_unbuilt_graphs()


test_setup = TestSession()


# TODO Avoid gym to allow for its exclusion as a dependency
# TODO Have these test cases conform to new agent-hypothesis standard
class TestHomoMultiAgentPipe(TestCase):
	def setUp(self):
		self.sess = test_setup.sess
		self.ob_space = test_setup.ob_space
		self.action_space = test_setup.action_space
		self.frame_factory = test_setup.frame_factory
		self.shared_hypothesis = test_setup.shared_hypothesis
		self.act_op = test_setup.act_op
		self.multiAgentPipe = test_setup.multiAgentPipe

	def on_build_graph(self, thread_index, thread_handler, multiAgentPipe: BatchAgentExplorer, sess: tf.Session,
					   frame_factory,
					   shared_hypothesis):
		queue = multiAgentPipe.queues[thread_index]

		with sess.graph.as_default():
			with tf.variable_scope('agent_' + str(thread_index)):
				env = gym.make('Assault-v0')
				workerseed = int(np.random.randint(1, 10000, dtype=np.uint32))
				env.seed(workerseed)
				gym.logger.setLevel(logging.WARN)
				action_space_shape = frame_factory.shapes[1]
				action_space_dtype = frame_factory.dtypes[1]
				env = Environment.wrap_env(env, action_space_shape, action_space_dtype)
				action_space = env.action_space

				agent = StatelessMonoSelectorTFAgent(sess, shared_hypothesis, [Stochastic(make_pdtype(action_space))])
				action_op, val_pred_op = agent.act([])

				placeholders = []
				for shape, dtype in zip(queue.shapes, queue.dtypes):
					placeholders.append(tf.placeholder(dtype, shape))
			return sess, agent, env, \
				   U.function(placeholders, queue.enqueue(placeholders)), \
				   action_op, val_pred_op

	def on_start(self, thread_index, thread_handler, sess: tf.Session, agent, env, enqueue, action_op,
				 val_pred_op):
		print("Thread %d: Starting" % thread_index)

		message_interface = MessageInterfaceType(thread_handler.message_interface)

		prev_ob = env.reset()

		with sess.as_default():
			while not thread_handler.should_stop():

				if message_interface.has_message():
					print("Thread %d: Awaiting hypothesis sync" % thread_index)
					message_interface.barrier(10)

				prev_agent_state = agent.get_state_copy()
				action, val_pred = sess.run([action_op, val_pred_op])  # No observation needed
				ob, reward, is_ep_start, _ = env.step(action)
				frame = self.frame_factory(
					prev_agent_state, action, 0, prev_ob, reward, val_pred, int(is_ep_start)
				)
				prev_ob = ob

				try:
					if thread_handler.should_stop():
						break
					enqueue(*frame.get_feed_list())
					if thread_handler.should_stop():
						break
				# print("Enqueued, queue size: %d" % q_size())
				except tf.errors.CancelledError as e:
					if not thread_handler.should_stop():
						raise e
				# if thread_index == 0:
				# 	env.render()
				else:
					sleep(0.1 * np.random.rand())
			env.close()
			print("Thread %d: Exiting on coordinator request" % thread_index)

	def test_trajectory_pipe(self):
		try:
			with self.sess.as_default():
				self.sess.run(tf.global_variables_initializer())
				self.multiAgentPipe.start()

				q_size_ops = self.multiAgentPipe.each_queue(lambda queue, index: queue.size())
				self.consume_from_pipe(q_size_ops)
		except Exception as e:
			print(e)
			self.assertTrue(False)

	def test_hypothesis_propagator(self):
		self.hypothesis_propagator = CoordinatedRandomPropagator(self.sess, self.action_space, self.shared_hypothesis,
																 self.multiAgentPipe)
		try:
			with self.sess.as_default():
				self.sess.run(tf.global_variables_initializer())
				self.multiAgentPipe.start()
				self.hypothesis_propagator.start_propagation()

				q_size_ops = self.multiAgentPipe.each_queue(lambda queue, index: queue.size())
				self.consume_from_pipe(q_size_ops)
				print('Requesting hypothesis propagator to stop')
				self.hypothesis_propagator.request_stop()
		except Exception as e:
			print(e)
			self.assertTrue(False)
		print('Waiting for hypothesis propagator to stop')
		self.hypothesis_propagator.thread.join()
		print('Test complete')

	def test_experience(self):
		try:
			q_size_ops = self.multiAgentPipe.each_queue(lambda queue, index: queue.size())
			experience = CircularStackedExperience(4)
			with self.sess.as_default():
				self.sess.run(tf.global_variables_initializer())
				self.multiAgentPipe.start()

				thread = threading.Thread(target=self.sample_from_experience, args=[experience])
				thread.start()
				self.consume_from_pipe(q_size_ops, experience)
				print('Joining on experience sampler')
				thread.join(timeout=2)
		except Exception as e:
			print(e)
			self.assertTrue(False)

	def tearDown(self):
		print("Tearing down")
		self.multiAgentPipe.stop_all(thread_stop_mode='destroy', queue_stop_mode='destroy')

	def consume_from_pipe(self, q_size_ops, experience: Experience = None):
		for i in range(10):
			sleep(0.1 * random.random())
			frames = [TestHomoMultiAgentPipe.RLFrameClass(*dequeue) for dequeue in
					  self.sess.run(self.multiAgentPipe.dequeue_all)]
			if experience is not None:
				experience.add(frames)
			for frame in frames:
				count = np.count_nonzero(frame.ob)
				frame.ob = count
			q_sizes = self.sess.run(q_size_ops)
			if np.count_nonzero(q_sizes) != 4:
				print('Starvation observed: ', q_sizes)
		print("Consumption complete")

	def sample_from_experience(self, experience: Experience):
		try:
			sampler = CircularSequentialManySampler(experience, 2, next_timeout=1)
			print('Getting next sample')
			print(sampler.__next__())
			for sample, i in zip(sampler, range(2)):
				print('Recollecting experience sample %d:' % i, sample)
		except TimeoutError as e:
			print(e)
			# TODO Fix: self.assertTrue(False) does not cause test case failure when called from separate thread.
			self.assertTrue(False)


if __name__ == '__main__':
	unittest.main()
