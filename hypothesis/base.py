import tensorflow as tf

from sandblox import TFObject, tf_method, TFMethod
from ..utils.distributions import make_pdtype


class Hypothesis(object):
	def __init__(self, ob_space, ac_space, **kwargs):
		super(Hypothesis, self).__init__(**kwargs)

		self.ob_space = ob_space
		self.ac_space = ac_space

		self.pd_type = make_pdtype(ac_space)

	def behave(self, observation, curr_state):
		raise NotImplementedError

	def new_state(self):
		raise NotImplementedError


class TFHypothesis(TFObject, Hypothesis):
	def __init__(self, sess: tf.Session, ob_space, ac_space, scope_name=None):
		super(TFHypothesis, self).__init__(scope_name=scope_name, ob_space=ob_space, ac_space=ac_space)
		self.sess = sess

	class BehaveMethod(TFMethod):
		def __init__(self, name: str, obj: [object, TFObject], method, override_inputs, *args, **kwargs):
			super(TFHypothesis.BehaveMethod, self).__init__(name, obj, method, override_inputs, *args, **kwargs)
			self.assign_trainable_vars = obj.assign_trainable_vars

	@tf_method(tf_method_class=BehaveMethod)
	def behave(self, observation, curr_state):
		raise NotImplementedError(
			"behave has not been defined, ensure that the hypothesis has been built correctly by it's builder")

	def assign_state(self, src_state, dest_state):
		raise NotImplementedError

	def new_state(self, batch_shape: [None, int, list] = None):
		raise NotImplementedError

	def new_state_placeholder(self, batch_shape: [None, int, list] = None):
		raise NotImplementedError

	def new_state_variable(self, batch_shape: [None, int, list] = None):
		raise NotImplementedError

	def assign_trainable_vars(self, other):
		if self.behave.is_frozen():
			return self.behave.assign_trainable_vars(other)
		else:
			raise TypeError("behave is not frozen, might cause anomalous conditions")
