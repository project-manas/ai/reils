import tensorflow as tf

from sandblox import tf_method, Outs
from ..hypothesis.base import TFHypothesis
from ..utils import tf_util as U


class SimpleTFHypothesis(TFHypothesis):
	def __init__(self, sess,
				 ob_space,
				 ac_space,
				 kind='small',
				 name: str = None):
		self.kind = kind
		super(SimpleTFHypothesis, self).__init__(sess, ob_space, ac_space, name)

	def new_state_placeholder(self, batch_shape: [None, int, list] = None):
		return tf.placeholder(tf.float32, ())

	def new_state_variable(self, batch_shape: [None, int, list] = None):
		return tf.Variable(tf.constant(0, dtype=tf.float32))

	def new_state(self, batch_size: [None, int, list] = None):
		return 0

	def assign_state(self, dest_state, src_state):
		pass

	@tf_method()
	def behave(self, observation, curr_state):
		x = observation  # [?, time_length, env]
		x = tf.divide(x, [.2, 2., .2, 2.])
		x = U.clip(x, -1, 1)
		shapes_tf = tf.shape(x)
		ob_shape = list(map(int, x.get_shape()[2:]))
		x = tf.reshape(x, [-1] + ob_shape)
		outputs = U.dense(x, 256, "fc1")

		with tf.variable_scope('logits'):
			logits = U.dense(outputs, self.pd_type.param_shape()[0], "logits", U.normc_initializer(0.1))
			logits = tf.reshape(logits, (shapes_tf[0], shapes_tf[1], self.pd_type.param_shape()[0]))

		with tf.variable_scope('value_pred'):
			value_pred = U.dense(outputs, 1, "value", U.normc_initializer(0.1))
			value_pred = tf.reshape(value_pred, (shapes_tf[0], shapes_tf[1], 1))[:, :, 0]

		return Outs(('logits', logits), ('value_pred', value_pred), ('next_state', tf.constant(0, dtype=tf.float32)))
