import numpy as np
import tensorflow as tf

from sandblox import tf_method, Out
from ..hypothesis.base import TFHypothesis
from ..utils import tf_util as U


class RnnTFHypothesis(TFHypothesis):
	def __init__(self, sess,
			ob_space,
			ac_space,
			kind = 'small',
			n_hidden_cells = 128, name: str = None):
		self.kind = kind
		self.n_hidden_cells = n_hidden_cells
		super(RnnTFHypothesis, self).__init__(sess, ob_space, ac_space, name)

	@staticmethod
	def to_shape(batch_shape: [None, int, list] = None) -> list:
		if batch_shape is None:
			return []
		elif isinstance(batch_shape, int):
			return [batch_shape]
		else:
			return batch_shape

	def new_state_placeholder(self, batch_shape: [None, int, list] = None):
		return tf.placeholder(shape = RnnTFHypothesis.to_shape(batch_shape) + [2, self.n_hidden_cells],
			dtype = tf.float32,
			name = type(self).__name__ + '_state')

	def new_state_variable(self, batch_shape: [None, int, list] = None):
		return tf.Variable(
			tf.zeros(RnnTFHypothesis.to_shape(batch_shape) + [2, self.n_hidden_cells], dtype = tf.float32),
			name = self.scope.rel + '_state')

	def new_state(self, batch_size: [None, int, list] = None):
		return np.zeros(RnnTFHypothesis.to_shape(batch_size) + [2, self.n_hidden_cells], np.float32)

	def assign_state(self, dest_state, src_state):
		return tf.assign(dest_state, src_state, name = self.scope.abs + '/assign_state')

	@tf_method()
	def behave(self, observation, curr_state):
		x = observation / 255.0  # [?, time_length, env]
		shapes_tf = tf.shape(x)
		ob_shape = list(map(int, x.get_shape()[2:]))
		x = tf.reshape(x, [-1] + ob_shape)
		x = self.cnn_block(x, kind = self.kind)
		x = tf.reshape(x, (shapes_tf[0], shapes_tf[1], int(x.get_shape()[1])))  # [?, time_length, logits]

		cell = tf.contrib.rnn.LSTMCell(self.n_hidden_cells, state_is_tuple = True)

		curr_state = tf.transpose(curr_state, [1, 0] + list(range(2, curr_state.shape.ndims)))
		cell_in, hidden_in = tf.unstack(curr_state)
		state = tf.contrib.rnn.LSTMStateTuple(cell_in, hidden_in)

		outputs, state = tf.nn.dynamic_rnn(cell, x, dtype = tf.float32,
			initial_state = state)  # [?, time_length, hidden_cells]
		cell_out, hidden_out = state

		with tf.variable_scope('next_state'):
			next_state = tf.stack([cell_out, hidden_out])

		shapes = outputs.get_shape()
		shapes_tf = tf.shape(outputs)
		outputs = tf.reshape(outputs, (-1, int(shapes[2])))

		with tf.variable_scope('logits'):
			logits = U.dense(outputs, self.pd_type.param_shape()[0], "logits", U.normc_initializer(0.01))
			logits = tf.reshape(logits, (shapes_tf[0], shapes_tf[1], self.pd_type.param_shape()[0]))
		with tf.variable_scope('value_pred'):
			value_pred = U.dense(outputs, 1, "value", U.normc_initializer(1.0))
			value_pred = tf.reshape(value_pred, (shapes_tf[0], shapes_tf[1], 1))[:, :, 0]

		next_state = tf.transpose(next_state, [1, 0] + list(range(2, curr_state.shape.ndims)))

		return Out(('logits', logits), ('value_pred', value_pred), ('next_state', next_state))

	@staticmethod
	def cnn_block(x, kind):
		if kind == 'small':  # from A3C paper
			x = tf.nn.relu(U.conv2d(x, 16, "l1", [8, 8], [4, 4], pad = "VALID"))
			x = tf.nn.relu(U.conv2d(x, 32, "l2", [4, 4], [2, 2], pad = "VALID"))
			x = U.flattenallbut0(x)
			x = tf.nn.relu(U.dense(x, 256, 'lin', U.normc_initializer(1.0)))
		elif kind == 'large':  # Nature DQN
			x = tf.nn.relu(U.conv2d(x, 32, "l1", [8, 8], [4, 4], pad = "VALID"))
			x = tf.nn.relu(U.conv2d(x, 64, "l2", [4, 4], [2, 2], pad = "VALID"))
			x = tf.nn.relu(U.conv2d(x, 64, "l3", [3, 3], [1, 1], pad = "VALID"))
			x = U.flattenallbut0(x)
			x = tf.nn.relu(U.dense(x, 512, 'lin', U.normc_initializer(1.0)))
		else:
			raise NotImplementedError

		return x
