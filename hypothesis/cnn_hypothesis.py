import tensorflow as tf

from sandblox import tf_method, Out
from ..hypothesis.base import TFHypothesis
from ..utils import tf_util as U


class CnnTFHypothesis(TFHypothesis):
	def __init__(self, sess,
				 ob_space,
				 ac_space,
				 kind='small',
				 name: str = None):
		self.kind = kind
		super(CnnTFHypothesis, self).__init__(sess, ob_space, ac_space, name)

	def new_state_placeholder(self, batch_shape: [None, int, list] = None):
		return tf.placeholder(tf.float32, ())

	def new_state_variable(self, batch_shape: [None, int, list] = None):
		return tf.Variable(tf.constant(0, dtype=tf.float32))

	def new_state(self, batch_size: [None, int, list] = None):
		return 0

	def assign_state(self, dest_state, src_state):
		pass

	@tf_method()
	def behave(self, observation, curr_state):
		x = observation / 255.0  # [?, time_length, env]
		shapes_tf = tf.shape(x)
		ob_shape = list(map(int, x.get_shape()[2:]))
		x = tf.reshape(x, [-1] + ob_shape)
		outputs = self.cnn_block(x, kind=self.kind)

		with tf.variable_scope('logits'):
			logits = U.dense(outputs, self.pd_type.param_shape()[0], "logits", U.normc_initializer(0.01))
			logits = tf.reshape(logits, (shapes_tf[0], shapes_tf[1], self.pd_type.param_shape()[0]))

		with tf.variable_scope('value_pred'):
			value_pred = U.dense(outputs, 1, "value", U.normc_initializer(1.0))
			value_pred = tf.reshape(value_pred, (shapes_tf[0], shapes_tf[1], 1))[:, :, 0]

		return Out(('logits', logits), ('value_pred', value_pred), ('next_state', tf.constant(0, dtype=tf.float32)))

	@staticmethod
	def cnn_block(x, kind):
		if kind == 'small':  # from A3C paper
			x = tf.nn.relu(U.conv2d(x, 16, "l1", [8, 8], [4, 4], pad="VALID"))
			x = tf.nn.relu(U.conv2d(x, 32, "l2", [4, 4], [2, 2], pad="VALID"))
			x = U.flattenallbut0(x)
			x = tf.nn.relu(U.dense(x, 256, 'lin', U.normc_initializer(1.0)))
		elif kind == 'large':  # Nature DQN
			x = tf.nn.relu(U.conv2d(x, 32, "l1", [8, 8], [4, 4], pad="VALID"))
			x = tf.nn.relu(U.conv2d(x, 64, "l2", [4, 4], [2, 2], pad="VALID"))
			x = tf.nn.relu(U.conv2d(x, 64, "l3", [3, 3], [1, 1], pad="VALID"))
			x = U.flattenallbut0(x)
			x = tf.nn.relu(U.dense(x, 512, 'lin', U.normc_initializer(1.0)))
		else:
			raise NotImplementedError
		return x
