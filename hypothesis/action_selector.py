import tensorflow as tf


class ActionSelector(object):
	"""
	Selects an action from a continuous or discrete probability distribution
	:param pdtype: Type of probability distribution, e.g.: Gaussian, Categorical
	"""

	def __init__(self, pdtype, *args):
		self.pdtype = pdtype

	def __call__(self, *args, **kwargs):
		raise NotImplementedError("Must return action given action distribution")


class Stochastic(ActionSelector):
	def __init__(self, pdtype):
		super(Stochastic, self).__init__(pdtype)

	def __call__(self, logits):
		pd = self.pdtype.pdfromflat(logits)
		return pd.sample()


class Greedy(ActionSelector):
	def __init__(self, pdtype):
		super(Greedy, self).__init__(pdtype)

	def __call__(self, logits):
		pd = self.pdtype.pdfromflat(logits)
		return pd.mode()


class EpsilonGreedy(ActionSelector):
	def __init__(self, pdtype, epsilon):
		self.epsilon = tf.Variable(epsilon, dtype=tf.float32, name="epsilon", trainable = False)
		super(EpsilonGreedy, self).__init__(pdtype)

	def __call__(self, logits):
		pd = self.pdtype.pdfromflat(logits)
		rand = tf.random_uniform(shape=(), minval=0.0, maxval=1.0, dtype=tf.float32)
		action = tf.cond(rand < self.epsilon, lambda: pd.sample(), lambda: pd.mode())
		return action


class _DecayingEpsilonGreedy(EpsilonGreedy):
	def __init__(self, pdtype, epsilon, max_timesteps):
		self.epsilon_initial = tf.constant(epsilon, dtype=tf.float32, name="epsilon_initial")
		self.time_step = tf.Variable(0.0, dtype=tf.float32, name="timesteps")
		# TODO Use global timestep instead!
		self.max_timesteps = tf.constant(max_timesteps, dtype=tf.float32, name="max_timesteps")
		super(_DecayingEpsilonGreedy, self).__init__(pdtype, epsilon)

	def __call__(self, logits):
		self.epsilon = tf.maximum(0.0, self.epsilon)
		return super(_DecayingEpsilonGreedy, self).__call__(logits)


class LinDecayEpsilonGreedy(_DecayingEpsilonGreedy):
	def __init__(self, pdtype, epsilon, max_timesteps):
		super(LinDecayEpsilonGreedy, self).__init__(pdtype, epsilon, max_timesteps)

	def __call__(self, logits):
		self.time_step = tf.assign_add(self.time_step, 1)
		self.epsilon = (1.0 - self.time_step / self.max_timesteps) * self.epsilon_initial
		return super(LinDecayEpsilonGreedy, self).__call__(logits)


class ExpDecayEpsilonGreedy(_DecayingEpsilonGreedy):
	def __init__(self, pdtype, epsilon, max_timesteps):
		super(ExpDecayEpsilonGreedy, self).__init__(pdtype, epsilon, max_timesteps)

	def __call__(self, logits):
		raise NotImplementedError("Exponentially decaying epsilon")
