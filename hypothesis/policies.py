import tensorflow as tf

from reils.env import Space
from sandblox import Out, tf_method, Outs
from ..hypothesis.base import TFHypothesis


# TODO Conform predefined policies to new Hypothesis Standard

class UniformRandomTFHypothesis(TFHypothesis):
	def __init__(self, action_space):
		super(UniformRandomTFHypothesis, self).__init__(None, None, action_space)
		self.get_pd_value = None

	def assign_trainable_vars(self, hypothesis):
		return tf.no_op()

	def on_behave(self, observation, curr_state: [tf.Operation, None]):
		n = self.action_space.shape[0]
		# TODO Support higher dimensional action spaces
		logits = tf.constant([1.0 / self.action_space.n for _ in range(n)], dtype=tf.float32)
		val = tf.constant(0.0, tf.float32)
		return logits, val


class BiasedRandomTFHypothesis(TFHypothesis):
	# def __init__(self, action_space: Space, bias_logits, name: str):
	# 	self.logits = tf.get_variable(name, action_space.shape, action_space.dtype, bias_logits)
	def __init__(self, sess: tf.Session, action_space: Space, biases=None, scope_name: str = None):
		super(BiasedRandomTFHypothesis, self).__init__(sess, None, action_space, scope_name=scope_name)

		initializer = biases if biases is not None else tf.random_uniform(action_space.shape, 0, 1, action_space.dtype)
		self.biases = tf.get_variable('%s_weights' % self.scope.rel, initializer=initializer)

	@tf_method()
	def behave(self, observation, curr_state=None):
		logits = self.biases / tf.reduce_sum(self.biases)
		return Outs(('logits', logits), ('v_pred', tf.constant(0.0)), ('next_state', tf.constant(0.0)))
