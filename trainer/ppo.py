import tensorflow as tf

from ..agent.frames import FrameFactory
from ..feeder.batch_feeder import BatchFeeder
from ..trainer.base import TFTrainer
from ..utils import tf_util as U


class PPO(TFTrainer):
	def __init__(self,
				 feed_pipe: BatchFeeder,
				 hypothesis,
				 frame_factory: FrameFactory,
				 clip_param=0.2,
				 entropy_coeff=0.05,
				 value_coeff=0.5,
				 max_grad_norm=None,
				 scope_name: str = None,
				 type: str = 'rnn'):
		self.frame = frame_factory
		self.clip_param = clip_param
		self.entropy_coeff = entropy_coeff
		self.value_coeff = value_coeff
		self.max_grad_norm = max_grad_norm
		self.type = type
		self.pi = None
		self.pi_old = None
		super(PPO, self).__init__(feed_pipe, hypothesis, scope_name)

	def _loss(self):
		with tf.variable_scope('deque'):
			dq_op = self.iterator.dequeue()

			ac = dq_op[self.frame['ac']]
			atarg = dq_op[self.frame['adv']]
			ob = dq_op[self.frame['ob']]
			ret = dq_op[self.frame['rew']]
			curr_state = dq_op[self.frame['state']]

			if self.type == 'rnn':
				curr_state = curr_state[:, 0]
				curr_state = tf.squeeze(curr_state, axis=1)

		with tf.variable_scope('current'):
			self.pi = self.hypothesis_type.behave(ob, curr_state)
			pd = self.hypothesis_type.pd_type.pdfromflat(self.pi.o.logits)

		with tf.variable_scope('previous'):
			self.pi_old = self.hypothesis_type.behave(ob, curr_state)
			pd_old = self.hypothesis_type.pd_type.pdfromflat(self.pi_old.o.logits)

		clip_param = self.clip_param * self.multiplier

		with tf.variable_scope('metrics'):
			self.graph.hypothesis_entropy = self.entropy_coeff * U.mean(pd.entropy())
			self.graph.mean_kl_divergence = U.mean(pd_old.kl(pd)) + 1e-7

		with tf.variable_scope('hypothesis'):
			ratio = tf.exp(pd.logp(ac) - pd_old.logp(ac) + 1e-7)
			hypothesis_surrogate = ratio * atarg
			hypothesis_clipped = U.clip(ratio, 1.0 - clip_param, 1.0 + clip_param) * atarg
			self.graph.hypothesis_loss = U.mean(tf.minimum(hypothesis_clipped, hypothesis_surrogate))

		with tf.variable_scope('value'):
			value_clipped = self.pi_old.o.value_pred + tf.clip_by_value(self.pi.o.value_pred - self.pi_old.o.value_pred,
																		-clip_param,
																		clip_param)
			value_loss_1 = tf.square(self.pi.o.value_pred - ret)
			value_loss_2 = tf.square(value_clipped - ret)
			self.graph.value_loss = .5 * self.value_coeff * U.mean(tf.maximum(value_loss_1, value_loss_2))

		with tf.variable_scope('total_loss'):
			loss = - self.graph.hypothesis_loss \
				   - self.graph.hypothesis_entropy \
				   + self.graph.value_loss

		return loss

	def _train_step(self, optimizer, loss):
		params = self.pi.get_trainable_variables()
		gradients = tf.gradients(loss, params)
		if self.max_grad_norm is not None:
			gradients, norm = tf.clip_by_global_norm(gradients, self.max_grad_norm)
		gradients = list(zip(gradients, params))
		lr = self.learning_rate * self.multiplier
		# TODO Trainers are not responsible for instantiating optimizers
		train = optimizer(lr).apply_gradients(gradients)
		return train, loss
