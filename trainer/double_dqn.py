import tensorflow as tf
from tensorflow.python.training.optimizer import Optimizer

from reils import TFTrainer, FrameFactory
from reils.utils import tf_util


class DoubleDQN(TFTrainer):
	def __init__(self, frame_factory: FrameFactory, iterator: tf.data.Iterator, hypothesis_type,
				 double_dqn: bool = True, gamma=0.99, scope_name=None):
		super(DoubleDQN, self).__init__(frame_factory, iterator, hypothesis_type, scope_name)
		self.double_dqn = double_dqn
		self.gamma = gamma

	def _loss(self):
		frame = self.frame_factory
		with tf.variable_scope('deque'):
			dq_op = self.iterator.get_next()

			ob = dq_op[frame['ob']]
			curr_state = dq_op[frame['state']]
			ac = dq_op[frame['ac']]
			r = dq_op[frame['rew']]
			start = dq_op[frame['start']]
			next_ob = dq_op[frame['next_ob']]
			next_state = dq_op[frame['next_state']]

			batch_size = tf.cast(tf.shape(start)[0], tf.float32)

		non_terminal = tf.cast(1 - start, tf.float32)

		# if self.type == 'rnn':
		# 	curr_state = curr_state[:, 0]
		# 	curr_state = tf.squeeze(curr_state, axis=1)

		with tf.variable_scope('learning'):
			self.q_s = self.hypothesis_type.behave(ob, curr_state)
			q_s_vals = self.q_s.o.logits
			q_s_a_val = tf.reduce_sum(q_s_vals * tf.one_hot(ac, q_s_vals.shape[-1]), len(q_s_vals.shape) - 1,
									  name='q_s_a_val')

		if self.double_dqn:
			with tf.variable_scope('learning', reuse=True):
				self.q_s1 = self.hypothesis_type.behave(next_ob, next_state)

		with tf.variable_scope('target'):
			self.qt = self.hypothesis_type.behave(next_ob, next_state)

			qt_vals = self.qt.o.logits

			if self.double_dqn:
				# Q(s, a) <- r(s,a) + gamma * Q'(s', argmax a' Q(s', a'))
				q_s1_ac = tf.argmax(self.q_s1.o.logits, axis=len(self.q_s1.o.logits.shape) - 1, name='q_s1_ac')
				target_q_val = tf.reduce_sum(qt_vals * tf.one_hot(q_s1_ac, qt_vals.shape[-1]), len(qt_vals.shape) - 1,
											 name='target_q_s1_a1_val')
			else:
				# Q(s, a) <- r(s,a) + gamma * Q'(s', argmax a' Q'(s', a')) - Q(s, a) = r(s,a) + gamma * max a' { Q'(s', a') }
				target_q_val = tf.reduce_max(qt_vals, name='target_qt_val')

			target = tf.stop_gradient(r + non_terminal * (self.gamma * target_q_val))

		with tf.variable_scope('td_error'):
			td_error = target - q_s_a_val
			loss = tf_util.huber_loss(td_error)

		return tf.reduce_sum(loss) / batch_size

	def _train_step(self, optimizer: Optimizer, loss):
		train = optimizer.minimize(loss)
		return train, loss
