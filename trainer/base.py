import tensorflow as tf
from tensorflow.python.training.optimizer import Optimizer

from reils import Hypothesis, FrameFactory
from sandblox import TFObject


class Trainer(object):
	def __init__(self, frame_factory: FrameFactory, iterator: tf.data.Iterator, hypothesis_type: Hypothesis, **kwargs):
		self.frame_factory = frame_factory
		self.iterator = iterator
		self.hypothesis_type = hypothesis_type
		super(Trainer, self).__init__()

	def loss(self, *args):
		raise NotImplementedError

	def train_step(self, *args):
		raise NotImplementedError


# TODO Implement the multi-hypothesis architecture found in Driving Imitation, one network with initial state, one without
# TODO Make it a builder just like Agent and Hypothesis
class TFTrainer(TFObject, Trainer):
	class Graph(TFObject):
		def __init__(self, scope_name: str = None):
			super(TFTrainer.Graph, self).__init__(scope_name)
			self.train_step = None
			self.loss = None

		def is_built(self):
			return all(op is not None for op in [self.train_step, self.loss])

	def __init__(self, frame_factory: FrameFactory, iterator, hypothesis_type, scope_name: str = None):
		super(TFTrainer, self).__init__(
			frame_factory=frame_factory, iterator=iterator, hypothesis_type=hypothesis_type, scope_name=scope_name)
		self.graph = self.Graph(self.scope.abs)
		# TODO learning_rate and multiplier are aspects of optimizer which are not meant to be a part of the trainer
		self.learning_rate = tf.placeholder(tf.float32, ())
		self.multiplier = tf.placeholder(tf.float32, ())

	def loss(self):
		with tf.variable_scope(self.scope.abs + "/loss"):
			self.graph.loss = self._loss()
			return self.graph.loss

	def _loss(self):
		raise NotImplementedError("Implement the graph for calculating loss")

	def train_step(self, optimizer: Optimizer):
		if self.graph.loss is None:
			self.loss()
		with tf.variable_scope(self.scope.abs + "/train_step"):
			self.graph.train_step = self._train_step(optimizer, self.graph.loss)
			return self.graph.train_step

	def _train_step(self, optimizer: Optimizer, loss):
		raise NotImplementedError("Implement the graph for returning the train step")
