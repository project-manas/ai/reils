import tensorflow as tf

from ..agent.frames import FrameFactory
from ..feeder.batch_feeder import BatchFeeder
from ..hypothesis.base import Hypothesis
from ..trainer.base import TFTrainer
from ..utils import tf_util as U


class PolicyGradient(TFTrainer):
	def __init__(self,
				 feed_pipe: BatchFeeder,
				 hypothesis_type: Hypothesis,
				 frame_factory: FrameFactory,
				 entropy_coeff=0.05,
				 value_coeff=0.5,
				 max_grad_norm=None,
				 scope_name: str = None,
				 is_recurrent: bool = True):
		self.frame = frame_factory
		self.entropy_coeff = entropy_coeff
		self.value_coeff = value_coeff
		self.max_grad_norm = max_grad_norm
		self.is_recurrent = is_recurrent
		self.pi = None
		super(PolicyGradient, self).__init__(feed_pipe, hypothesis_type, scope_name)

	def _loss(self):
		with tf.variable_scope('deque'):
			dq_op = self.iterator.dequeue()

			ac = dq_op[self.frame['ac']]
			atarg = dq_op[self.frame['adv']]
			ob = dq_op[self.frame['ob']]
			ret = dq_op[self.frame['rew']]
			curr_state = dq_op[self.frame['state']]

			if self.is_recurrent:
				curr_state = curr_state[:, 0]
				curr_state = tf.squeeze(curr_state, axis=1)

		self.pi = self.hypothesis_type.behave(ob, curr_state)
		pd = self.hypothesis_type.pd_type.pdfromflat(self.pi.o.logits)

		with tf.variable_scope('metrics'):
			self.graph.hypothesis_entropy = U.mean(pd.entropy())

		with tf.variable_scope('hypothesis'):
			self.graph.hypothesis_loss = U.mean(atarg * pd.logp(ac))

		with tf.variable_scope('value'):
			self.graph.value_loss = U.mean(tf.square(self.pi.o.value_pred - ret))

		with tf.variable_scope('total_loss'):
			loss = -self.graph.hypothesis_loss \
				   - self.entropy_coeff * self.graph.hypothesis_entropy \
				   + self.value_coeff * self.graph.value_loss

		return loss

	def _train_step(self, optimizer, loss):
		params = self.pi.get_trainable_variables()
		gradients = tf.gradients(loss, params)
		if self.max_grad_norm is not None:
			gradients, norm = tf.clip_by_global_norm(gradients, self.max_grad_norm)
		gradients = list(zip(gradients, params))
		lr = self.learning_rate * self.multiplier
		train = optimizer(lr).apply_gradients(gradients)
		return train, loss
