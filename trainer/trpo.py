import numpy as np
import tensorflow as tf

from reils import FrameFactory
from ..feeder.batch_feeder import BatchFeeder
from ..trainer.base import TFTrainer
from ..utils import tf_util as U


class NaturalGradientDescentOptimizer:
	def __init__(self, pi, pd, loss_func, cg_damping, max_kl):
		self.loss_func = loss_func
		self.loss, _ = loss_func()
		self.pi = pi
		self.var_list = pi.get_trainable_variables()
		self.pd = pd
		self.cg_damping = cg_damping
		batch_size = tf.cast(tf.shape(self.loss), tf.float32)
		kl_first = pd.self_kl() / batch_size
		self.policy_grads = self.flatgrad(self.loss, self.var_list)
		self.kl_first_grads = tf.gradients(kl_first, self.var_list)
		self.shapes = list(map(self.var_shape, self.var_list))
		self.max_kl = max_kl

	def minimize(self):
		stepdir = self.conjugate_descent(-self.policy_grads)
		shs = 0.5 * tf.tensordot(stepdir, self.fisher_vector_product(stepdir), axes=1)
		lm = tf.sqrt(shs / self.max_kl)
		fullstep = stepdir / lm

		rate = tf.tensordot(-self.policy_grads, stepdir, axes=1) / lm
		theta_new = self.linesearch(fullstep, rate)

		assign_op = self.pi.assign_trainable_vars_directly(theta_new)

		return assign_op

	def linesearch(self, fullstep, expected_improve_rate):
		accept_ratio = 0.1
		max_backtracks = 10

		current_grads = self.pi.get_trainable_variables()
		loss = self.loss_func()

		def check(count, actual_improve, stepsize):
			expected_improve = expected_improve_rate * stepfrac
			ratio = actual_improve / expected_improve
			return count < max_backtracks or ratio < accept_ratio or actual_improve < 0

		def body(count, actual_improve, stepsize):
			stepsize /= 2
			count += 1
			x_new = current_grads + stepsize * fullstep
			self.pi.assign_trainable_vars_directly(x_new)
			new_loss = self.loss_func()
			actual_improve = loss - new_loss
			return [count, actual_improve, stepsize]

		stepfrac = tf.Variable(tf.constant(1, dtype=tf.float32))
		actual_improve = tf.Variable(tf.constant(0, dtype=tf.float32))
		count = tf.Variable(tf.constant(0, dtype=tf.float32))

		_, _, stepsize = tf.while_loop(check, body, [count, actual_improve, stepfrac])

		self.pi.assign_trainable_vars_directly(current_grads)
		return current_grads + stepsize * fullstep

	def conjugate_descent(self, b, iters=10, residual_tol=1e-7):
		p = tf.identity(b)
		r = tf.identity(b)
		x = tf.Variable(tf.zeros(b.get_shape(), tf.float32))
		r_sq = tf.tensordot(r, r, axes=1)
		count = tf.Variable(tf.constant(0, dtype=tf.float32))

		def check(count, p, r, x, r_sq):
			return tf.logical_or(tf.less(count, 10), tf.less(residual_tol, r_sq))

		def body(count, p, r, x, r_sq):
			z = self.fisher_vector_product(p)
			v = r_sq / tf.tensordot(p, z, axes=1)
			x_new = x + (v * p)
			r_new = r - (v * z)
			r_sq_new = tf.tensordot(r_new, r_new, axes=1)
			mu = r_sq_new / r_sq
			p_new = r_new + mu * p
			return [count + 1, p_new, r_new, x_new, r_sq_new]

		_, _, _, x, _ = tf.while_loop(check, body, [count, p, r, x, r_sq])
		return x

	def fisher_vector_product(self, p):
		tangents = self.unflatten(p, self.shapes)
		gvp = [tf.reduce_sum(g * t) for (g, t) in
			   zip(self.kl_first_grads, tangents) if g is not None]

		return self.flatgrad(gvp, self.var_list) + self.cg_damping * p

	def unflatten(self, vector, shapes):
		elems = []
		start = 0
		for shape in shapes:
			size = np.prod(shape)
			elem = tf.reshape(vector[start:(start + size)], shape)
			elems.append(elem)

		return elems

	def var_shape(self, x):
		out = [k.value for k in x.get_shape()]
		assert all(isinstance(a, int) for a in out)
		return out

	def numel(self, x):
		return np.prod(self.var_shape(x))

	def flatgrad(self, loss, var_list):
		grads = tf.gradients(loss, var_list)
		return tf.concat([
			tf.reshape(
				grad, [self.numel(v)]
			)
			for (v, grad) in zip(var_list, grads) if grad is not None
		], 0)

	def flatten(self, x):
		return tf.concat([
			tf.reshape(v, [self.numel(v)]
					   )
			for v in x
		], 0)


class TRPO(TFTrainer):
	def __init__(self, feed_pipe: BatchFeeder, hypothesis_type, frame: FrameFactory,
				 entropy_coeff=0.05, value_coeff=0.5, cg_clamp=0, max_kl=0, scope_name: str = None,
				 type: str = 'rnn'):
		self.frame = frame
		self.entropy_coeff = entropy_coeff
		self.value_coeff = value_coeff
		self.cg_clamp = cg_clamp
		self.max_kl = max_kl
		self.type = type
		self.pi = None
		self.pi_old = None
		super(TRPO, self).__init__(feed_pipe, hypothesis_type, scope_name)

	def _loss(self):
		dq_op = self.iterator.dequeue()

		ac = dq_op[self.frame['ac']]
		atarg = dq_op[self.frame['adv']]
		ob = dq_op[self.frame['ob']]
		ret = dq_op[self.frame['rew']]
		curr_state = dq_op[self.frame['state']]

		if self.type == 'rnn':
			curr_state = curr_state[:, 0]
			curr_state = tf.squeeze(curr_state, axis=1)

		with tf.variable_scope('current'):
			self.pi = self.hypothesis_type.behave(ob, curr_state, )
		with tf.variable_scope('previous'):
			self.pi_old = self.hypothesis_type.behave(ob, curr_state)

		self.loss_func = lambda: self._loss_internal(self.pi, self.pi_old, ac, atarg, ret)

		pol, val = self.loss_func()
		return pol + val

	def _loss_internal(self, pi, pi_old, ac, atarg, ret):
		pd = self.hypothesis_type.pd_type.pdfromflat(pi.out.logits)
		pd_old = self.hypothesis_type.pd_type.pdfromflat(pi_old.out.logits)

		policy_entropy = U.mean(pd.entropy())

		ratio = tf.exp(pd.logp(ac) - pd_old.logp(ac) + 1e-7)
		policy_loss = U.mean(ratio * atarg)

		value_loss = tf.square(pi.out.value_pred - ret)
		value_loss = .5 * U.mean(value_loss)

		return - policy_loss - self.entropy_coeff * policy_entropy, self.value_coeff * value_loss

	def _train_step(self, optimizer, loss):
		optimizer = NaturalGradientDescentOptimizer(self.pi,
													self.hypothesis_type.pd_type.pdfromflat(self.pi.out.logits),
													self.loss_func,
													self.cg_clamp,
													self.max_kl)
		return optimizer.minimize()
