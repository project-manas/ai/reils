from reils import TFTrainer, BatchFeeder


class DQN(TFTrainer):
	def __init__(self, feeder: BatchFeeder, hypothesis_type, scope_name=None):
		super(DQN, self).__init__(feeder, hypothesis_type, scope_name)
