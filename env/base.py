import numpy as np
import tensorflow


# TODO Write tests!
class Space(object):
	def __init__(self, shape=None, dtype=None):
		self.shape = None if shape is None else tuple(shape)
		self.dtype = None if dtype is None else np.dtype(dtype)

	def contains(self, sample):
		raise NotImplementedError

	def sample(self):
		raise NotImplementedError

	@staticmethod
	def as_tf_dtype(dtype):
		if dtype == int:
			return tensorflow.int32
		elif dtype == float:
			return tensorflow.int32
		else:
			return NotImplementedError('No known conversion for specified dtype: ', dtype)


class Discrete(Space):
	def __init__(self, high):
		self.high = np.array(high)
		assert self.high.dtype == np.int
		super(Discrete, self).__init__(self.high.shape, self.high.dtype)

	def contains(self, x):
		return x.shape == self.shape and (x >= 0).all() and (x <= self.high).all()

	def sample(self):
		return np.random.uniform(np.zeros(self.shape, self.dtype), self.high, self.shape)


class Continuous(Space):
	def __init__(self, low, high):
		self.low = low if isinstance(low, np.ndarray) else np.array(low)
		self.high = high if isinstance(high, np.ndarray) else np.array(high)
		assert self.low.shape == self.high.shape
		assert self.low.dtype == self.high.dtype
		super(Continuous, self).__init__(self.high.shape, self.high.dtype)

	def contains(self, x):
		return x.shape == self.low.shape and (x >= self.low).all() and (x <= self.high).all()

	def sample(self):
		return np.random.uniform(self.low, self.high, self.low.shape)


class SpaceTuple(Space):
	def __init__(self, spaces: [Space]):
		self.spaces = spaces
		super(SpaceTuple, self).__init__(self.foreach(lambda space: space.shape),
										 self.foreach(lambda space: space.dtype))

	def contains(self, samples):
		all(self.foreach(lambda space, sample: space.contains(sample), samples))

	def sample(self):
		return self.foreach(lambda space: space.sample())

	def foreach(self, func, zip_arg=None):
		iterable = self.spaces if zip is None else zip(self.spaces, zip_arg)
		return tuple([func(space) for space in iterable])


class Spaces(object):
	Discrete = Discrete
	Continuous = Continuous
	SpaceTuple = SpaceTuple


# TODO Expand environment to accommodate all environment categorizations (Eg: Fully Observable vs. Partially Observable, Static vs. Dynamic, etc)
class Environment(object):
	def __init__(self, ob_space: Space, ac_space: Space):
		self.ob_space = ob_space
		self.ac_space = ac_space

	def step(self, action):
		raise NotImplementedError("Must return observation and reward")

	def reset(self):
		raise NotImplementedError("Must return initial unconditional observation at start of episode")

	def conditional_reset(self, is_ep_start, ob):
		return self.reset() if is_ep_start else ob

	def render(self):
		raise NotImplementedError

	def close(self):
		raise NotImplementedError

	def _fix_dim(self, value):
		shape = value.shape

	@staticmethod
	def get_spaces(env):
		return env.observation_space, env.action_space
